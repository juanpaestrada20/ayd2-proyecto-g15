CREATE DATABASE ayd2_proyecto1;
USE ayd2_proyecto1;

CREATE TABLE Usuario(
    id_usuario integer AUTO_INCREMENT,
    nickname varchar(100) NOT NULL UNIQUE,
    correo varchar(300) NOT NULL UNIQUE,
    contra varchar(300) NOT NULL,
    fecha_nacimiento date NOT NULL,
    tipo integer,
    
    CONSTRAINT PK_ID_USUARIO PRIMARY KEY(id_usuario)
);

CREATE TABLE Carpeta(
    id_carpeta integer AUTO_INCREMENT,
    nombre varchar(100) NOT NULL,
    fecha_creacion date NOT NULL,
    id_padre integer,
    id_usuario integer NOT NULL,
    
    CONSTRAINT PK_ID_CARPETA PRIMARY KEY(id_carpeta),
    CONSTRAINT FK_ID_CARPETA_CARPETA FOREIGN KEY(id_padre) REFERENCES Carpeta (id_carpeta) ON DELETE CASCADE,
    CONSTRAINT FK_ID_USUARIO_CARPETA FOREIGN KEY(id_usuario) REFERENCES Usuario (id_usuario) ON DELETE CASCADE
);

CREATE TABLE Archivo(
    id_archivo integer AUTO_INCREMENT,
    nombre varchar(100) NOT NULL,
    nombre_almacenamiento varchar(200) NOT NULL,
    extencion varchar(10),
    fecha_subida date,
    size integer NOT NULL,
    id_carpeta integer NOT NULL,
    id_usuario integer NOT NULL,
    
    CONSTRAINT PK_ID_ARCHIVO PRIMARY KEY(id_archivo),
    CONSTRAINT FK_ID_CARPETA_ARCHIVO FOREIGN KEY(id_carpeta) REFERENCES Carpeta (id_carpeta) ON DELETE CASCADE,
    CONSTRAINT FK_ID_USUARIO_ARCHIVO FOREIGN KEY(id_usuario) REFERENCES Usuario (id_usuario) ON DELETE CASCADE
);

CREATE TABLE Log(
    id_log integer AUTO_INCREMENT,
    ruta varchar(200) NOT NULL,
    metodo varchar(100) NOT NULL,
    entrada varchar(500) NOT NULL,
    salida varchar(500) NOT NULL,
    es_error boolean NOT NULL,
    fecha_hora datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT PK_ID_LOG PRIMARY KEY(id_log)
);
