const env = require('./config.js');
const mysql = require('mysql');

const connection = mysql.createConnection({
    host: env.MYSQL_HOST || 'localhost',
    user: env.MYSQL_USER || 'curioso',
    password: env.MYSQL_PASSWORD || 'curioso123',
    port: env.MYSQL_PORT || 3306,
    database: env.MYSQL_DB || 'ayd2_proyecto1'
});

connection.connect();

module.exports = connection;
