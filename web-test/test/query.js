const { query } = require('./mysql.js');
const connection = require('./mysql.js');

class Observable {
    constructor() {
        this.observers = [];
    }

    // Suscribe una clase notificadora
    subscribe(c) {
        this.observers.push(c);
    }

    // Desuscribe la clase notificadora
    unsubscribe(c) {
        this.observers = this.observers.filter(observer => observer instanceof c !== true);
    }

    // Llama a todos nuestros suscriptores
    notify(model) {
        this.observers.forEach(observer => {
            observer.notify(model);
        });
    }
}

class userObserver extends Observable {
	constructor() {
        super();
		this.dbquery = null;
		this.params = [];
    }

	setQuery(dbquery, params){
		this.dbquery = dbquery;
		this.params = [params];
		this.notify(this);
	}
}

class userHandlerProd {
	notify(model){
		return new Promise((data) => {
			connection.query(model.dbquery, model.params, function (error, result) {
				try {
					if (error) throw error;
					data({ success: true, result });
				} catch (error) {
					data({ success: false, error });
				}
			});
		});
	}
}


exports.userObserver = userObserver;
exports.userHandlerProd =  userHandlerProd;