const connection = require('./mysql.js');


module.exports = class userHandler {
    async deleteUser(nickname){
        const query = 'DELETE FROM Usuario WHERE nickname = ? ';
	    const params = [nickname];
	    try {
		    await this.execQuery(query, params);
	    } catch (err) {
		    console.log(err);
	    }

    }

    execQuery(databaseQuery, params){
		return new Promise((data) => {
			connection.query(databaseQuery, params, function (error, result) {
				try {
					if (error) throw error;
					data({ success: true, result });
				} catch (error) {
					data({ success: false, error });
				}
			});
		});
	}
}