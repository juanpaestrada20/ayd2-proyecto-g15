const { Builder, By, until } = require('selenium-webdriver');
const firefox = require('selenium-webdriver/firefox');
const { expect } = require('chai');
const env = require('./config');
const {userObserver, userHandlerProd} = require('./query');
let userDeleter = new userObserver();
userDeleter.subscribe(new userHandlerProd());

const HOST = env.PAGE_HOST;

// Dummy data
const folder = 'Folder Prueba';
const user = 'PruebaUser';
const pass = 'PruebaPass';

describe('User manegment test', () => {
    const options = new firefox.Options();
    const driver = new Builder()
        .forBrowser('firefox')
        .setFirefoxOptions(options)
        .build();

    it('Delete user', async () => {
        userDeleter.setQuery('DELETE FROM Usuario WHERE nickname = ? ', user)
    });

    it('Login fail', async () => {
        await driver.get(`${HOST}`);
        await driver.wait(until.elementLocated(By.className('login-container')));
        await driver.findElement(By.name('nickname')).sendKeys(user);
        await driver.findElement(By.name('contra')).sendKeys(pass);
        await driver.findElement(By.className('login-button')).click();
        await driver.sleep(2000);
        const msg = await driver.getCurrentUrl();
        expect(msg).to.equal(`${HOST}/`);
    });

    it('Register succesfully', async () => {
        await driver.get(`${HOST}/registro`);
        await driver.wait(until.elementLocated(By.className('login-container')));
        await driver.findElement(By.name('nickname')).sendKeys(user);
        await driver.findElement(By.name('contra')).sendKeys(pass);
        await driver.findElement(By.name('validar_contra')).sendKeys(pass);
        await driver.findElement(By.name('correo')).sendKeys('prueba@ayd2.com');
        await driver.findElement(By.name('fecha_nacimiento'));
        await driver.findElement(By.name('telefono')).sendKeys('00000000');
        await driver.findElement(By.className('login-button')).click();
        await driver.sleep(8000);
        const msg = await driver.getCurrentUrl();
        expect(msg).to.equal(`${HOST}/`);
    });

    it('Login succesfully', async () => {
        await driver.get(`${HOST}`);
        await driver.wait(until.elementLocated(By.className('login-container')));
        await driver.findElement(By.name('nickname')).sendKeys(user);
        await driver.findElement(By.name('contra')).sendKeys(pass);
        await driver.findElement(By.className('login-button')).click();
        await driver.sleep(3000);
        const msg = await driver.getCurrentUrl();
        expect(msg).to.equal(`${HOST}/archivos`);
    });

    it('Create folder', async () => {
        const button = await driver.findElements(By.name('selenium-button-folder'));
        await button[0].click();
        await driver.sleep(1500);
        await driver.findElement(By.className('form-control')).sendKeys(folder);
        await driver.findElement(By.className('btn btn-primary')).click();
        await driver.sleep(3000);
        const folderName = await driver.findElement(By.name(`selenium-input-${folder}`)).getAttribute('value');
        expect(folderName).to.equal(folder);
    });

    it('Delete folder', async () => {
        const optbutton = await driver.findElements(By.name(`selenium-show-${folder}`));
        await optbutton[0].click();
        await driver.sleep(1500);
        const delbutton = await driver.findElements(By.name(`selenium-delete-${folder}`));
        await delbutton[0].click();
        await driver.sleep(2000);
        const folderContent = await driver.findElement(By.xpath('//tbody')).getText()
        expect(folderContent).to.equal('')
        await driver.close()
    });

})