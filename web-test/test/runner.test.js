const { Builder, By, until } = require('selenium-webdriver');
const { expect } = require('chai');
const User = require('./query2');
const userHandler = new User();
const env = require('./config');

const USERNAME = env.LT_USERNAME;
const KEY = env.LT_ACCESS_KEY;
const HOST = env.PAGE_HOST;

// gridUrl: gridUrl can be found at automation dashboard
const GRID_HOST = 'hub.lambdatest.com/wd/hub';

// Setup Input capabilities
const capabilities = {
    platform: 'Windows 10',
    browserName: 'Chrome',
    version: '92.0',
    resolution: '1024x768',
    network: true,
    visual: true,
    console: true,
    video: true,
    name: 'Test', // name of the test
    build: 'NodeJS build' // name of the build
}

// Dummy data
const user = 'PruebaUser2';
const pass = 'PruebaPass';
const folder = 'Folder Prueba';

// URL: https://{username}:{accessKey}@hub.lambdatest.com/wd/hub
const gridUrl = 'https://' + USERNAME + ':' + KEY + '@' + GRID_HOST;
describe('webdriver', () => {
    const driver = new Builder()
        .usingServer(gridUrl)
        .withCapabilities(capabilities)
        .build();

    it('Delete user', async () => {
        await userHandler.deleteUser(user);
    });

    it('Login fail', async () => {
        await driver.get(`${HOST}`);
        await driver.wait(until.elementLocated(By.className('login-container')));
        await driver.findElement(By.name('nickname')).sendKeys(user);
        await driver.findElement(By.name('contra')).sendKeys(pass);
        await driver.findElement(By.className('login-button')).click();
        await driver.sleep(2000);
        const msg = await driver.getCurrentUrl();
        expect(msg).to.equal(`${HOST}/`);
    });

    it('Register succesfully', async () => {
        await driver.get(`${HOST}/registro`);
        await driver.wait(until.elementLocated(By.className('login-container')));
        await driver.findElement(By.name('nickname')).sendKeys(user);
        await driver.findElement(By.name('contra')).sendKeys(pass);
        await driver.findElement(By.name('validar_contra')).sendKeys(pass);
        await driver.findElement(By.name('correo')).sendKeys('prueba@ayd2.com');
        await driver.findElement(By.name('fecha_nacimiento'));
        await driver.findElement(By.name('telefono')).sendKeys('00000000');
        await driver.findElement(By.className('login-button')).click();
        await driver.sleep(8000);
        const msg = await driver.getCurrentUrl();
        expect(msg).to.equal(`${HOST}/`);
    });

    it('Login succesfully', async () => {
        await driver.get(`${HOST}`);
        await driver.wait(until.elementLocated(By.className('login-container')));
        await driver.findElement(By.name('nickname')).sendKeys(user);
        await driver.findElement(By.name('contra')).sendKeys(pass);
        await driver.findElement(By.className('login-button')).click();
        await driver.sleep(3000);
        const msg = await driver.getCurrentUrl();
        expect(msg).to.equal(`${HOST}/archivos`);
    });

    it('Create folder', async () => {
        const button = await driver.findElements(By.name('selenium-button-folder'));
        await button[0].click();
        await driver.sleep(1500);
        await driver.findElement(By.className('form-control')).sendKeys(folder);
        await driver.findElement(By.className('btn btn-primary')).click();
        await driver.sleep(3000);
       // const folderName = await driver.findElement(By.name(`selenium-input-${folder}`)).getAttribute('value');
       // expect(folderName).to.equal(folder);
    });

    it('Delete folder', async () => {
        const optbutton = await driver.findElements(By.name(`selenium-show-${folder}`));
        await optbutton[0].click();
        await driver.sleep(1500);
        const delbutton = await driver.findElements(By.name(`selenium-delete-${folder}`));
        await delbutton[0].click();
        await driver.sleep(2000);
        const folderContent = await driver.findElement(By.xpath('//tbody')).getText()
        expect(folderContent).to.equal('')
        await driver.close()
    });
})
