const env = require('./config.js');
const express = require('express');
const cors = require('cors');
const connection = require('./mysql.js');

const { json, urlencoded } = express;
const app = express();
const PORT = env.SERVER_PORT || 3000;
const HOST = env.SERVER_HOST || 'localhost';

const corsOptions = {
	origin: '*',
	optionsSuccessStatus: 200,
};

app.use(json());
app.use(urlencoded({ extended: false }));
app.use(cors(corsOptions));

app.put('/user', async (req, res) => {
	const { nickname, correo, phone, fecha_nacimiento, contra, tipo, auth_email, auth_phone } = req.body;
	const query =
		"UPDATE Usuario SET correo = ?, phone = ?, fecha_nacimiento = DATE_FORMAT(STR_TO_DATE(?, '%d-%m-%Y'), '%Y-%m-%d'), tipo = ?, auth_email = ?, auth_phone = ? WHERE nickname = ? AND contra = ?";
	const params = [correo, phone, fecha_nacimiento, tipo, auth_email, auth_phone, nickname, contra];
	try {
		const result = await execQuery(query, params);
		if (result.success)
			result.result.changedRows > 0
				? res.send({ success: true, msg: 'Usuario actualizado!' })
				: res.send({
						success: false,
						msg: 'Ningún usuario fue actualizado!',
				  });
		else throw result.error;
	} catch (err) {
		res.status(500).json(err);
	}
});

app.put('/user/contra', async (req, res) => {
	const { nickname, contra, contra_nueva } = req.body;
	const resultExistencia = await verifyExistency(nickname, contra);
	try {
		if (!resultExistencia.success && resultExistencia.error)
			throw resultExistencia.error;
		if (!resultExistencia.success && resultExistencia.msg)
			res.send({ success: false, msg: resultExistencia.msg });
		else {
			const query = 'UPDATE Usuario SET contra = ? WHERE id_usuario = ?';
			const params = [contra_nueva, resultExistencia.id_usuario];
			const result = await execQuery(query, params);
			if (result.success)
				result.result.changedRows > 0
					? res.send({
							success: true,
							msg: 'Contraseña actualizada!',
					  })
					: res.send({
							success: false,
							msg: 'Ninguna contraseña fue actualizada!',
					  });
			else throw result.error;
		}
	} catch (err) {
		res.status(500).json(err);
	}
});

const verifyExistency = async (nickname, contra) => {
	const query =
		'SELECT id_usuario FROM Usuario WHERE nickname = ? AND contra = ?';
	const params = [nickname, contra];
	try {
		const result = await execQuery(query, params);
		if (result.success)
			return result.result[0]
				? { success: true, id_usuario: result.result[0].id_usuario }
				: { success: false, msg: 'Usuario no encontrado!' };
		else throw result.error;
	} catch (err) {
		return { success: false, error };
	}
};

function execQuery(databaseQuery, params) {
	return new Promise((data) => {
		connection.query(databaseQuery, params, function (error, result) {
			try {
				if (error) throw error;
				data({ success: true, result });
			} catch (error) {
				data({ success: false, error });
			}
		});
	});
}

app.listen(PORT, () => console.log(`Listening on PORT: ${PORT}`));
