let server = require('./index');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let should = chai.should();

chai.use(chaiHttp);
describe('Crear Usuario', () => {
	const usuario = {
		nickname: 'userTest',
		correo: 'userTest@gmail.com',
		phone: '31776050',
		contra: 'userTest123',
		fecha_nacimiento: '20-07-1999',
		tipo: 0,
	};

	// Elimina el usuario en caso ya exista
	beforeEach(async() => {
		const query = "DELETE FROM Usuario WHERE nickname = ?";
		const params = [usuario.nickname];
		await server.execQuery(query, params)
	});
	/*
	 * Ruta a testear /user con metodo POST
	 */
	describe('/user POST', () => {
		it('Deberia de crear un usuario', (done) => {
			chai.request(server.app)
				.post('/user')
				.send(usuario)
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.be.a('object');
					res.body.should.have.property('success').eql(true);
					res.body.should.have.property('msg').eql('Usuario creado!');
					done();
				});
		});
	});
});
