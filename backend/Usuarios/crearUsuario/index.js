const env = require('./config.js');
const express = require('express');
const cors = require('cors');
const expressAsyncHandler = require('express-async-handler');
const connection = require('./mysql.js');

const { json, urlencoded } = express;
const app = express();
const PORT = env.SERVER_PORT || 3000;
const HOST = env.SERVER_HOST || 'localhost';

const corsOptions = {
	origin: '*',
	optionsSuccessStatus: 200,
};

app.use(json());
app.use(urlencoded({ extended: false }));
app.use(cors(corsOptions));

app.post(
	'/user',
	expressAsyncHandler(async (req, res) => {
		const { nickname, correo, phone, contra, fecha_nacimiento, tipo } = req.body;
		const query =
			"INSERT INTO Usuario(nickname, correo, phone, contra, fecha_nacimiento, tipo) VALUES (?, ?, ?, ?, DATE_FORMAT(STR_TO_DATE(?, '%d-%m-%Y'), '%Y-%m-%d'), ?)";
		const params = [nickname, correo, phone, contra, fecha_nacimiento, tipo];
		try {
			const result = await execQuery(query, params);
			if (result.success)
				res.send({
					success: true,
					msg: 'Usuario creado!',
					id: result.id,
				});
			else throw result.error;
		} catch (err) {
			if (err.sqlMessage.includes('nickname'))
				res.send({
					success: true,
					error: 'El nickname ya esta en uso!',
				});
			else if (err.sqlMessage.includes('correo'))
				res.send({
					success: false,
					error: 'El correo ya esta en uso!',
				});
			else res.status(500).json(err);
		}
	})
);

function execQuery(databaseQuery, params) {
	return new Promise((data) => {
		connection.query(databaseQuery, params, function (error, result) {
			try {
				if (error) throw error;
				data({ success: true, id: result.insertId });
			} catch (error) {
				data({ success: false, error });
			}
		});
	});
}

app.listen(PORT, () => console.log(`Listening on PORT: ${PORT}`));

module.exports = { app, execQuery };
