const nodemailer = require('nodemailer');
const env = require('./config.js');
const Cryptr = require('cryptr');

const transporter = nodemailer.createTransport({
	host: 'smtp.gmail.com',
	port: 465,
	secure: true,
	auth: {
		user: env.EMAIL,
		pass: env.EMAIL_PASS,
	},
});

async function sendEmail(token, to_user) {
	const cryptr = new Cryptr('p');
	const email = cryptr.decrypt(to_user);

	const mail = `<p>Tu código de seguridad es: <b>${token}</b></p>`;
	const mailOptions = {
		from: `"Kemofiles" <${env.EMAIL}>`,
		to: to_user,
		subject: 'Codigo de seguridad de Kemofiles',
		html: mail,
	};
	try {
		const a = await transporter.sendMail(mailOptions);
		console.log(a)
		return true;
	} catch (e) {
		console.log(e);
		return false;
	}
}

module.exports = sendEmail;
