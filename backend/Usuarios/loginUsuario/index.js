const env = require('./config.js');
const express = require('express');
const crypto = require('crypto');
const cors = require('cors');
const connection = require('./mysql.js');
const sendEmail = require('./email.js');
const client = require('twilio')(env.TWILIO_ACCOUNT_SID, env.TWILIO_AUTH_TOKEN);

const { json, urlencoded } = express;
const app = express();
const PORT = env.SERVER_PORT || 3000;
const HOST = env.SERVER_HOST || 'localhost';

const corsOptions = {
	origin: '*',
	optionsSuccessStatus: 200,
};

app.use(json());
app.use(urlencoded({ extended: false }));
app.use(cors(corsOptions));

app.post('/login', async (req, res) => {
	const { nickname, contra } = req.body;
	const query = `SELECT id_usuario, nickname, correo, phone, DATE_FORMAT(fecha_nacimiento, '%d-%m-%Y') as fecha_nacimiento, tipo, auth_email, auth_phone FROM Usuario WHERE nickname = ? AND contra = ?`;
	const params = [nickname, contra];
	try {
		const result = await execQuery(query, params);
		if (result.success) {
			if (result.user)
				await saveToken(
					result.user.auth_phone,
					result.user.auth_email,
					result.user.id_usuario,
					result.user.correo,
					result.user.phone
				);
			result.user
				? res.send({ user: result.user })
				: res.send({ user: null, msg: 'Usuario no encontrado' });
		} else throw result.error;
	} catch (err) {
		res.status(500).json(err);
	}
});

app.post('/validate', async (req, res) => {
	const { id_usuario, token_email, token_phone } = req.body;
	try {
		const query =
			'SELECT auth_email, auth_phone, token_email, token_phone FROM Usuario WHERE id_usuario = ? AND NOW() < token_lifetime';
		const params = [id_usuario];
		const result = await execQuery(query, params);
		console.log(result);
		if (result.user) {
			if (
				result.user.auth_email == 1 &&
				result.user.token_email != token_email
			)
				res.send({
					success: false,
					msg: 'Token de correo inválido!',
				});
			else if (
				result.user.auth_phone == 1 &&
				result.user.token_phone != token_phone
			)
				res.send({
					success: false,
					msg: 'Token de teléfono inválido!',
				});
			else
				res.send({
					success: true,
					msg: 'Token válido!',
				});
		} else
			res.send({
				success: false,
				msg: 'Tiempo de token inválido!',
			});
	} catch (err) {
		res.status(500).json(err);
	}
});

app.post('/resend', async (req, res) => {
	const { id_usuario, auth_email, auth_phone } = req.body;
	const query = 'SELECT correo, phone FROM Usuario WHERE id_usuario = ?';
	const params = [id_usuario];

	try {
		const usuario = await execQuery(query, params);
		if (usuario.success) {
			if (usuario.user) {
				const result = await saveToken(
					auth_phone,
					auth_email,
					id_usuario,
					usuario.user.correo,
					usuario.user.phone
				);
				res.send({ success: result });
			}
			res.send({ success: false });
		} else throw usuario.error;
	} catch (err) {
		res.status(500).json(err);
	}
});

async function saveToken(auth_phone, auth_email, userId, email, phone) {
	let token_phone = null;
	let token_email = null;
	try {
		if (auth_phone == 1) {
			token_phone = generateToken();
			console.log(token_phone);
			try {
				console.log(phone);
				client.messages
					.create({
						body: `Tu código es: ${token_phone}`,
						from: '+18508054238',
						to: phone.includes('+502')
							? phone.trim()
							: `+502${phone.trim()}`,
					})
					.then((message) => console.log(message.sid)).catch(e=>console.log(e));
			} catch (e) {
				console.log(e);
			}
		}
		if (auth_email == 1) {
			token_email = generateToken();
			console.log(token_email);
			await sendEmail(token_email, email);
		}
		const query = `UPDATE Usuario SET token_email = ?, token_phone = ?, token_lifetime = DATE_ADD(NOW(), INTERVAL 3 MINUTE) WHERE id_usuario = ?`;
		const params = [token_email, token_phone, userId];
		const result = await execQuery(query, params);
		return result.success;
	} catch (err) {
		return false;
	}
}

function generateToken() {
	return crypto.randomBytes(20).toString('hex').slice(0, 6);
}

function execQuery(databaseQuery, params) {
	return new Promise((data) => {
		connection.query(databaseQuery, params, function (error, result) {
			try {
				if (error) throw error;
				data({ success: true, user: result[0] });
			} catch (error) {
				data({ success: false, error });
			}
		});
	});
}

app.listen(PORT, () => console.log(`Listening on PORT: ${PORT}`));

module.exports = { app, execQuery };
