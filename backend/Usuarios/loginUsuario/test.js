let server = require('./index');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let should = chai.should();

chai.use(chaiHttp);
describe('Login Usuario', () => {
	const usuario = {
		nickname: 'userTest',
		correo: 'userTest@gmail.com',
		contra: 'userTest123',
		fecha_nacimiento: '20-07-1999',
		tipo: 0,
	};

	//Crea el usuario en caso no exista
	beforeEach(async() => {
		const query =
			"INSERT INTO Usuario(nickname, correo, contra, fecha_nacimiento, tipo) VALUES (?, ?, ?, DATE_FORMAT(STR_TO_DATE(?, '%d-%m-%Y'), '%Y-%m-%d'), ?)";
		const params = [usuario.nickname, usuario.correo, usuario.contra, usuario.fecha_nacimiento, usuario.tipo];
		await server.execQuery(query, params)
	});
	/*
	 * Ruta a testear /login con metodo POST
	 */
	describe('/login POST', () => {
		it('Deberia de logguearse un usuario', (done) => {
			chai.request(server.app)
				.post('/login')
				.send(usuario)
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.be.a('object');
					res.body.should.have.property('user').should.be.a('object');
					done();
				});
		});
	});

    //Elimina el usuario al finalizar test
	afterEach(async() => {
		const query = "DELETE FROM Usuario WHERE nickname = ?";
		const params = [usuario.nickname];
		await server.execQuery(query, params)
	});
});
