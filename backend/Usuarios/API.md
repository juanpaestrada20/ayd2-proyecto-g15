# Crear Usuario

## Método

-   POST

## Ruta

-   34.136.7.97:3002/user

## Ejemplo Request

```json
{
	"nickname": "juanpa",
	"correo": "juanpa@gmail.com",
	"phone": "31776050",
	"contra": "juanpa123",
	"fecha_nacimiento": "20-07-1999",
	"tipo": 0
}
```

## Posibles Respuestas

```json
{
	"success": true,
	"msg": "Usuario creado!",
	"id": 35
}
```

```json
{
	"success": false,
	"error": "El correo ya esta en uso!"
}
```

```json
{
	"success": true,
	"error": "El nickname ya esta en uso!"
}
```

# Login Usuario

## Método

-   POST

## Ruta

-   34.136.7.97:3000/login

## Ejemplo Request

```json
{
	"nickname": "juanpablo",
	"contra": "juanpa123"
}
```

## Posibles Respuestas

```json
{
    "user": {
        "id_usuario": 98,
        "nickname": "juanpablo",
        "correo": "juanpa200799@gmail.com",
        "phone": "31776050",
        "fecha_nacimiento": "20-07-1999",
        "tipo": 0,
        "auth_email": 0,
        "auth_phone": 0
    }
}
```

```json
{
	"user": null,
	"msg": "Usuario no encontrado"
}
```

# Actualizar Usuario

## Método

-   PUT

## Ruta

-   34.136.7.97:3003/user

## Ejemplo Request

```json
{
    "nickname": "juanpablo",
    "correo": "juanpa200799@gmail.com",
    "phone": "31776050",
    "contra": "juanpa123",
    "fecha_nacimiento": "20-07-1999",
    "tipo": 0,
    "auth_email": 0,
    "auth_phone": 1
}
```

## Posibles Respuestas

```json
{
	"success": true,
	"msg": "Usuario actualizado!"
}
```

```json
{
	"success": false,
	"msg": "Nada fue actualizado!"
}
```

# Actualizar Contraseña

## Método

-   PUT

## Ruta

-   34.136.7.97:3003/user/contra

## Ejemplo Request

```json
{
	"nickname": "juanpa",
	"contra": "juanpa123",
	"contra_nueva": "juanpa1234"
}
```

## Posibles Respuestas

```json
{
	"success": true,
	"msg": "Contraseña actualizada!"
}
```

```json
{
	"success": false,
	"msg": "Usuario no encontrado!"
}
```

```json
{
	"success": false,
	"msg": "Ninguna contraseña fue actualizada!"
}
```

# Eliminar Usuario

## Método

-   DELETE

## Ruta

-   34.136.7.97:3001/user

## Ejemplo Request

```json
{
	"nickname": "juanpa",
	"contra": "juanpa123"
}
```

## Posibles Respuestas

```json
{
	"success": true,
	"msg": "Usuario eliminado!"
}
```

```json
{
	"success": false,
	"msg": "Ningún usuario fue eliminado!"
}
```

# Re-enviar código

## Método

-   POST

## Ruta

-   34.136.7.97:3000/resend

## Ejemplo Request

```json
{
    "id_usuario": 98,
    "auth_email": 1,
    "auth_phone": 1
}
```

## Posibles Respuestas

```json
{
	"success": true
}
```

```json
{
	"success": false
}
```

# Validar Token

## Método

-   POST

## Ruta

-   34.136.7.97:3000/validate

## Ejemplo Request

```json
{
    "id_usuario": 98,
    "token_email": "b596ea",
    "token_phone": "7d5573"
}
```

## Posibles Respuestas

```json
{
	"success": true,
	"msg": "Ingreso válido!"
}
```

```json
{
	"success": false,
	"msg": "Token de telefono inválido!"
}
```

```json
{
	"success": false,
	"msg": "Token de correo inválido!"
}
```

```json
{
	"success": false,
	"msg": "Tiempo de token inválido!"
}
```