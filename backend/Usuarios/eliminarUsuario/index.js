const env = require('./config.js');
const express = require('express');
const cors = require('cors');
const connection = require('./mysql.js');

const { json, urlencoded } = express;
const app = express();
const PORT = env.SERVER_PORT || 3000;
const HOST = env.SERVER_HOST || 'localhost';

const corsOptions = {
	origin: '*',
	optionsSuccessStatus: 200,
};

app.use(json());
app.use(urlencoded({ extended: false }));
app.use(cors(corsOptions));

app.delete('/user', async (req, res) => {
	const { nickname, contra } = req.body;
	const query = 'DELETE FROM Usuario WHERE nickname = ? AND contra = ?';
	const params = [nickname, contra];
	try {
		const result = await execQuery(query, params);
		if (result.success)
			result.affectedRows > 0
				? res.send({ success: true, msg: 'Usuario eliminado!' })
				: res.send({
						success: false,
						msg: 'Ningún usuario fue eliminado!',
				  });
		else throw result.error;
	} catch (err) {
		res.status(500).json(err);
	}
});

function execQuery(databaseQuery, params) {
	return new Promise((data) => {
		connection.query(databaseQuery, params, function (error, result) {
			try {
				if (error) throw error;
				data({ success: true, affectedRows: result.affectedRows });
			} catch (error) {
				data({ success: false, error });
			}
		});
	});
}

app.listen(PORT, () => console.log(`Listening on PORT: ${PORT}`));

module.exports = { app, execQuery };