# Subir Varios Archivos

## Método

-   POST

## Ruta

-   18.222.6.82:3005/archivos/upload/:id_usuario/:id_carpeta

## Ejemplo Request

```json
{
	"file": [archivo1.pdf, archivo2.pdf, archivos3.pdf]
}
```

## Rspuestas Posibles

```json
{
	"savedFiles": [
		{
			"id_archivo": 26,
			"nombre": "uno.pdf",
			"nombre_almacenamiento": "4aef397fde2acf14cc57b984fddc7670",
			"extencion": "html",
			"fecha_subida": "Sun, 22 Aug 2021 14:24:15 GMT",
			"size": 186415,
			"id_carpeta": "1",
			"id_usuario": "26"
		},
		{
			"id_archivo": 27,
			"nombre": "dos.pdf",
			"nombre_almacenamiento": "a70fd1acbda3f772994ed273702215e1",
			"extencion": "pdf",
			"fecha_subida": "Sun, 22 Aug 2021 14:24:15 GMT",
			"size": 153939,
			"id_carpeta": "1",
			"id_usuario": "26"
		}
	],
	"unsavedFiles": [
		{
			"nombre": "tres.pdf",
			"error": "Ya existe un archivo con ese nombre en la misma carpeta!"
		}
	]
}
```

```json
{
	"savedFiles": [],
	"unsavedFiles": [
		{
			"nombre": "4.py",
			"error": "Tipo de archivo no es válido!"
		}
	]
}
```

# Eliminar Archivo

## Método

-   DELETE

## Ruta

-   18.222.6.82:3005/archivos/:id_usuario/:id_archivo

## Posibles Respuestas

```json
{
	"success": true,
	"msg": "Archivo borrado exitosamente!"
}
```

```json
{
	"success": false,
	"error": "El archivo a eliminar no fue encontrado!"
}
```

# Abrir Archivo (solo funciona con pdf)

## Método

-   GET

## Ruta

-   18.222.6.82:3005/archivos/view/:nombre_almacenamiento

# Descargar Archivo

## Método

-   GET

## Ruta

-   18.222.6.82:3005/archivos/download/:nombre_almacenamiento

# Obtener Lista de Archivos de 1 Carpeta

## Método

-   GET

## Ruta

-   18.222.6.82:3005/archivos/list/:id_carpeta/:page/:items

## Ejemplo Request

-   18.222.6.82:3005/archivos/list/1/3/5

## Posible Respuesta

```json
{
	"success": true,
	"items": [
		{
			"id_archivo": 14,
			"nombre": "CursosJuanPabloEstrada.pdf",
			"nombre_almacenamiento": "95256c3e348d1adfd7b211f6bbb255cd",
			"extencion": "pdf",
			"fecha_subida": "2021-08-22T00:00:00.000Z",
			"size": 311562,
			"id_carpeta": 1,
			"id_usuario": 26
		},
		{
			"id_archivo": 15,
			"nombre": "Datos de Registro.pdf",
			"nombre_almacenamiento": "b039bc9f4ad2f10cc8045712e93b9e47",
			"extencion": "pdf",
			"fecha_subida": "2021-08-22T00:00:00.000Z",
			"size": 60384,
			"id_carpeta": 1,
			"id_usuario": 26
		},
		{
			"id_archivo": 16,
			"nombre": "documento-electronico-21638.pdf",
			"nombre_almacenamiento": "22f793ee98d1c9d6e0919dff4e07040c",
			"extencion": "pdf",
			"fecha_subida": "2021-08-22T00:00:00.000Z",
			"size": 2675027,
			"id_carpeta": 1,
			"id_usuario": 26
		},
		{
			"id_archivo": 17,
			"nombre": "documento-electronico-21638_compressed.pdf",
			"nombre_almacenamiento": "6d89fcfe8732ef288557316eb64f460c",
			"extencion": "pdf",
			"fecha_subida": "2021-08-22T00:00:00.000Z",
			"size": 201583,
			"id_carpeta": 1,
			"id_usuario": 26
		},
		{
			"id_archivo": 18,
			"nombre": "DPI 1.jpeg",
			"nombre_almacenamiento": "a9d8ddf314969a354820f8a66dd9f92d",
			"extencion": "jpeg",
			"fecha_subida": "2021-08-22T00:00:00.000Z",
			"size": 96057,
			"id_carpeta": 1,
			"id_usuario": 26
		}
	]
}
```

```json
{
	"success": true,
	"items": []
}
```

# Actualizar Nombre Archivo

## Método

-   POST

## Ruta

-   18.222.6.82:3005/archivos/updateName

## Ejemplo Request

```json
{
	"id_archivo": 49,
	"nombre_nuevo": "Percy.jpg",
	"id_carpeta": 77
}
```

## Posibles Respuestas

```json
{
	"success": true,
	"msg": "Nombre de archivo actualizado!"
}
```

```json
{
    "success": false,
    "error": "Ya existe un archivo con ese nombre en la carpeta!"
}
```

# Actualizar Ubicación Archivo

## Método

-   POST

## Ruta

-   34.136.7.97:3005/archivos/updateFile/:id_usuario/:id_carpeta/:id_archivo

## Ejemplo Request

34.136.7.97:3005/archivos/updateFile/57/60/35

## Posibles Respuestas

```json
{
	"success": true,
	"msg": "Ubicación archivo actualizada!"
}
```

```json
{
	"success": false,
	"error": "Ningún archivo fue actualizado!"
}
```

```json
{
	"success": false,
	"error": "Archivo no encontrado!"
}
```
```json
{
	"success": false,
	"error": "En la carpeta destino hay un archivo con el mismo nombre!"
}
```

# Enviar archivo a papelera

## Método

-   POST

## Ruta

-   34.136.7.97:3005/archivos/trash

## Ejemplo Request

```json
{
    "id_usuario": 68,
    "id_archivo": 47
}
```

## Posibles Respuestas

```json
{
    "success": true,
    "msg": "Archivo enviado a papelera!"
}
```

```json
{
    "success": false,
    "error": "No se pudo enviar el archivo a la papelera!"
}
```

```json
{
	"success": false,
	"error": "Archivo no encontrado!"
}
```

# Restaurar archivo de papelera

## Método

-   POST

## Ruta

-   34.136.7.97:3005/archivos/restore

## Ejemplo Request

```json
{
    "id_usuario": 68,
    "id_archivo": 47
}
```

## Posibles Respuestas

```json
{
    "success": true,
    "msg": "Archivo enviado a restaurado!"
}
```

```json
{
    "success": false,
    "error": "No se pudo restaurar el archivo!"
}
```

```json
{
	"success": false,
	"error": "Archivo no encontrado!"
}
```
```json
{
    "success": false,
    "error": "Ya hay un archivo con el mismo nombre en la carpeta a restaurar!"
}
```

# Obtener Lista de Archivos en papelera

## Método

-   GET

## Ruta

-   18.222.6.82:3005/archivos/trash/:id_usuario

## Ejemplo Request

-   18.222.6.82:3005/archivos/trash/68

## Posible Respuesta

```json
{
    "success": true,
    "items": [
        {
            "id_archivo": 47,
            "nombre": "percy.jpg",
            "nombre_almacenamiento": "f27223b8ec70c434bbe0e82cbb5d2ca1",
            "extencion": "image/jpeg",
            "fecha_subida": "2021-09-12T00:00:00.000Z",
            "size": 106567,
            "status": 0,
            "id_carpeta": 77,
            "id_usuario": 68
        }
    ]
}
```

```json
{
	"success": true,
	"items": []
}
```