const express = require('express');
const multer = require('multer');
const fs = require('fs');
const api = express.Router();
const Archivos = require('./archivos.js');
const archivos = new Archivos();

const upload = multer({ dest: 'uploads/' });

api.post('/upload/:id_usuario/:id_carpeta', upload.any(), async (req, res) => {
	try {
		const files = req.files;
		const id_usuario = req.params.id_usuario;
		const id_carpeta = req.params.id_carpeta;
		const savedFiles = [];
		const unsavedFiles = [];
		let i = 0;
		for (const file of files) {
			const { success, saved, unsaved } = await archivos.save(
				id_usuario,
				id_carpeta,
				file
			);
			if (success) {
				savedFiles.push(saved);
			} else {
				fs.unlinkSync(`${__dirname}/uploads/${file.filename}`);
				unsavedFiles.push(unsaved);
			}
			i++;
		}
		res.send({ savedFiles, unsavedFiles });
	} catch (err) {
		console.log(err);
		res.status(500).json(err);
	}
});

api.delete('/:id_usuario/:id_archivo', async (req, res) => {
	try {
		const { success, msg, error } = await archivos.deleteFile(
			req.params.id_archivo,
			req.params.id_usuario
		);
		if (success) res.send({ success, msg });
		else res.send({ success, error });
	} catch (err) {
		console.log(err);
		res.status(500).json(err);
	}
});

api.get('/view/:nombre_almacenamiento', async (req, res) => {
	try {
		const { success, nombre, ext, error } = await archivos.getFile(
			req.params.nombre_almacenamiento
		);
		if (success) {
			const path = `${__dirname}/uploads/${req.params.nombre_almacenamiento}`;
			fs.readFile(path, (err, data) => {
				res.contentType(ext);
				res.setHeader(
					'Content-Disposition',
					'inline; filename=' + nombre
				);
				res.send(data);
			});
		} else res.send({ error });
	} catch (err) {
		console.log(err);
		res.status(500).json(err);
	}
});

api.get('/download/:nombre_almacenamiento', async (req, res) => {
	try {
		const { success, nombre, error } = await archivos.getFile(
			req.params.nombre_almacenamiento
		);
		if (success) {
			const path = `${__dirname}/uploads/${req.params.nombre_almacenamiento}`;

			res.setHeader(
				'Content-Disposition',
				'attachment; filename=' + nombre
			);
			res.setHeader('Content-Transfer-Encoding', 'binary');
			res.setHeader('Content-Type', 'application/octet-stream');

			res.sendFile(path);
		} else res.send({ error });
	} catch (err) {
		console.log(err);
		res.status(500).json(err);
	}
});

api.get('/list/:id_carpeta/:page/:items', async (req, res) => {
	try {
		const { success, items, error } = await archivos.listFiles(req.params);
		if (success) res.send({ success, items });
		else res.send({ success, error });
	} catch (err) {
		console.log(err);
		res.status(500).json(err);
	}
});

api.put('/updateName', async (req, res) => {
	try {
		const { success, msg, error } = await archivos.changeName(req.body);
		if (success) res.send({ success, msg });
		else res.send({ success, error });
	} catch (err) {
		console.log(err);
		res.status(500).json(err);
	}
});

api.post('/updateFile/:id_usuario/:id_carpeta/:id_archivo', async (req, res) => {
	try {
		const { success, msg, error } = await archivos.moveFile(req.params);
		if (success) res.send({ success, msg });
		else res.send({ success, error });
	} catch (err) {
		console.log(err);
		res.status(500).json(err);
	}
});

api.post('/trash', async (req, res) => {
	try {
		const { success, msg, error } = await archivos.sendTrash(req.body);
		if (success) res.send({ success, msg });
		else res.send({ success, error });
	} catch (err) {
		console.log(err);
		res.status(500).json(err);
	}
});

api.post('/restore', async (req, res) => {
	try {
		const { success, msg, error } = await archivos.restoreFile(req.body);
		if (success) res.send({ success, msg });
		else res.send({ success, error });
	} catch (err) {
		console.log(err);
		res.status(500).json(err);
	}
});

api.get('/trash/:id_usuario', async (req, res) => {
	try {
		const { success, items, error } = await archivos.listTrashFiles(req.params);
		if (success) res.send({ success, items });
		else res.send({ success, error });
	} catch (err) {
		console.log(err);
		res.status(500).json(err);
	}
});

module.exports = api;
