const express = require('express');
const cors = require('cors');
const routes = require('./routes.js');

const { json, urlencoded } = express;
const app = express();

const corsOptions = {
	origin: '*',
	optionsSuccessStatus: 200,
};

app.use(json());
app.use(urlencoded({ extended: false }));
app.use(cors(corsOptions));
app.use('/archivos', routes);

module.exports = app;
