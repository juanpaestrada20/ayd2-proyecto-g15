const app = require('./app.js');
const env = require('./config.js');

const PORT = env.SERVER_PORT || 3000;
const HOST = env.SERVER_HOST || 'localhost';

app.listen(PORT, () => console.log(`Listening on PORT: ${PORT}`));

module.exports = app;