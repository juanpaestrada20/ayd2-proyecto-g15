const fs = require('fs');
const connection = require('./mysql.js');

module.exports = class UserHandler {
	async save(id_usuario, id_carpeta, file) {
		const savedname = file.originalname;
		const resultExistencia = await this.verifyExistance(
			savedname,
			id_carpeta
		);
		try {
			if (!resultExistencia.success && resultExistencia.error)
				throw resultExistencia.error;
			if (resultExistencia.success && resultExistencia.exists)
				return {
					success: false,
					unsaved: {
						nombre: savedname,
						error: 'Ya existe un archivo con ese nombre en la misma carpeta!',
					},
				};
			if (
				[
					'js',
					'php',
					'asp',
					'aspx',
					'c',
					'sh',
					'bat',
					'py',
					'exe',
				].includes(file.originalname.split('.')[1])
			)
				return {
					success: false,
					unsaved: {
						nombre: savedname,
						error: 'Tipo de archivo no es válido!',
					},
				};
			else {
				const query =
					'INSERT INTO Archivo(nombre, nombre_almacenamiento, extencion, fecha_subida, size, id_carpeta, id_usuario) VALUES (?, ?, ?, NOW(), ?, ?, ?)';
				const params = [
					savedname,
					file.filename,
					file.mimetype,
					file.size,
					id_carpeta,
					id_usuario,
				];
				const result = await this.execQuery(query, params);
				if (result.success) {
					const saved = {
						id_archivo: result.result.insertId,
						nombre: savedname,
						nombre_almacenamiento: file.filename,
						extencion: file.mimetype.split('/')[1],
						fecha_subida: new Date().toUTCString(),
						size: file.size,
						id_carpeta,
						id_usuario,
					};
					return { success: result.success, saved };
				} else throw result.error;
			}
		} catch (error) {
			return {
				success: false,
				unsaved: { nombre: file.originalname, error },
			};
		}
	}

	async verifyExistance(nombre, id_carpeta) {
		const query =
			'SELECT * FROM Archivo WHERE nombre = ? AND id_carpeta = ? AND status = 1';
		const params = [nombre, id_carpeta];
		try {
			const result = await this.execQuery(query, params);
			if (result.success)
				return {
					success: true,
					exists: result.result.length > 0,
					archivo: result.result[0],
				};
			else throw result.error;
		} catch (error) {
			return {
				success: result.success,
				error,
			};
		}
	}

	async deleteFile(id_archivo, id_usuario) {
		const verifyExistance = await this.verifyExistancebyID(
			id_archivo,
			id_usuario
		);
		try {
			if (verifyExistance.error) throw verifyExistance.error;
			if (!verifyExistance.exists)
				return {
					success: false,
					error: 'El archivo a eliminar no fue encontrado!',
				};

			const query =
				'DELETE FROM Archivo WHERE id_archivo = ? AND id_usuario = ?';
			const params = [id_archivo, id_usuario];
			const result = await this.execQuery(query, params);
			if (result.success) {
				if (result.result.affectedRows > 0) {
					fs.unlinkSync(
						`${__dirname}/uploads/${verifyExistance.file.nombre_almacenamiento}`
					);
				}
				return result.result.affectedRows > 0
					? { success: true, msg: 'Archivo borrado exitosamente!' }
					: {
							success: false,
							error: 'EL archivo no se pudo eliminar!',
					  };
			} else throw result.error;
		} catch (error) {
			return {
				success: result.success,
				error,
			};
		}
	}

	async getFile(nombre_almacenamiento) {
		const query = 'SELECT * FROM Archivo WHERE nombre_almacenamiento = ?';
		const params = [nombre_almacenamiento];
		try {
			const result = await this.execQuery(query, params);
			if (result.success) {
				if (result.result.length > 0) {
					return {
						success: true,
						nombre: result.result[0].nombre,
						ext: result.result[0].extencion,
					};
				}
				return {
					success: false,
					error: 'El archivo no fue encontrado!',
				};
			} else throw result.error;
		} catch (error) {
			return {
				success: result.success,
				error,
			};
		}
	}

	async verifyExistancebyID(id_archivo, id_usuario) {
		const query =
			'SELECT * FROM Archivo WHERE id_archivo = ? AND id_usuario = ?';
		const params = [id_archivo, id_usuario];
		try {
			const result = await this.execQuery(query, params);
			if (result.success)
				return {
					success: true,
					exists: result.result.length > 0,
					file: result.result[0],
				};
			else throw result.error;
		} catch (error) {
			return {
				success: result.success,
				error,
			};
		}
	}

	async listFiles({ id_carpeta, page, items }) {
		const offset = (page - 1) * items;
		const query =
			'SELECT * FROM Archivo WHERE id_carpeta = ? AND status = 1 LIMIT ? OFFSET ?';
		const params = [id_carpeta, Number(items), Number(offset)];
		try {
			const result = await this.execQuery(query, params);
			if (result.success) return { success: true, items: result.result };
			else throw result.error;
		} catch (error) {
			return { success: false, error };
		}
	}

	async changeName({ id_archivo, nombre_nuevo, id_carpeta }) {
		const nombreDisponible = await this.verifyExistance(
			nombre_nuevo,
			id_carpeta
		);
		if (nombreDisponible.success && nombreDisponible.exists)
			return {
				success: false,
				error: 'Ya existe un archivo con ese nombre en la carpeta!',
			};
		if (!nombreDisponible.success)
			return { success: false, error: nombreDisponible.error };
		const query = 'UPDATE Archivo SET nombre = ? WHERE id_archivo = ?';
		const params = [nombre_nuevo, id_archivo];
		try {
			const result = await this.execQuery(query, params);
			if (result.success)
				return result.result.changedRows > 0
					? {
							success: true,
							msg: 'Nombre de archivo actualizado!',
					  }
					: {
							success: false,
							error: 'Ningún archivo fue actualizado!',
					  };
			else throw result.error;
		} catch (error) {
			return { success: false, error };
		}
	}

	async moveFile({ id_archivo, id_usuario, id_carpeta }) {
		const resultExistencia = await this.verifyExistancebyID(
			id_archivo,
			id_usuario
		);
		if (resultExistencia.success && !resultExistencia.exists)
			return {
				success: false,
				error: 'Archivo no encontrado!',
			};
		if (!resultExistencia.success)
			return { success: false, error: resultExistencia.error };
		const availableName = await this.verifyExistance(
			resultExistencia.file.nombre,
			id_carpeta
		);
		if (availableName.success && availableName.exists)
			return {
				success: false,
				error: 'En la carpeta destino hay un archivo con el mismo nombre!',
			};
		if (!availableName.success)
			return { success: false, error: availableName.error };
		const query = 'UPDATE Archivo SET id_carpeta = ? WHERE id_archivo = ?';
		const params = [id_carpeta, id_archivo];
		try {
			const result = await this.execQuery(query, params);
			if (result.success)
				return result.result.changedRows > 0
					? {
							success: true,
							msg: 'Ubicación archivo actualizada!',
					  }
					: {
							success: false,
							error: 'Ningún archivo fue actualizado!',
					  };
			else throw result.error;
		} catch (error) {
			return { success: false, error };
		}
	}

	async sendTrash({ id_archivo, id_usuario }) {
		const resultExistencia = await this.verifyExistancebyID(
			id_archivo,
			id_usuario
		);
		if (resultExistencia.success && !resultExistencia.exists)
			return {
				success: false,
				error: 'Archivo no encontrado!',
			};
		if (!resultExistencia.success)
			return { success: false, error: resultExistencia.error };
		const query = 'UPDATE Archivo SET status = 0 WHERE id_archivo = ?';
		const params = [id_archivo];
		try {
			const result = await this.execQuery(query, params);
			if (result.success)
				return result.result.changedRows > 0
					? {
							success: true,
							msg: 'Archivo enviado a papelera!',
					  }
					: {
							success: false,
							error: 'No se pudo enviar el archivo a la papelera!',
					  };
			else throw result.error;
		} catch (error) {
			return { success: false, error };
		}
	}

	async restoreFile({ id_archivo, id_usuario }) {
		const resultExistencia = await this.verifyExistancebyID(
			id_archivo,
			id_usuario
		);
		if (resultExistencia.success && !resultExistencia.exists)
			return {
				success: false,
				error: 'Archivo no encontrado!',
			};
		if (!resultExistencia.success)
			return { success: false, error: resultExistencia.error };
		const availableName = await this.verifyExistance(
			resultExistencia.file.nombre,
			resultExistencia.file.id_carpeta
		);
		if (availableName.success && availableName.exists)
			return {
				success: false,
				error: 'Ya hay un archivo con el mismo nombre en la carpeta a restaurar!',
			};
		if (!availableName.success)
			return { success: false, error: availableName.error };
		const query = 'UPDATE Archivo SET status = 1 WHERE id_archivo = ?';
		const params = [id_archivo];
		try {
			const result = await this.execQuery(query, params);
			if (result.success)
				return result.result.changedRows > 0
					? {
							success: true,
							msg: 'Archivo enviado a restaurado!',
					  }
					: {
							success: false,
							error: 'No se pudo restaurar el archivo!',
					  };
			else throw result.error;
		} catch (error) {
			return { success: false, error };
		}
	}

	async listTrashFiles({ id_usuario }) {
		const query =
			'SELECT * FROM Archivo WHERE id_usuario = ? AND status = 0';
		const params = [id_usuario];
		try {
			const result = await this.execQuery(query, params);
			if (result.success) return { success: true, items: result.result };
			else throw result.error;
		} catch (error) {
			return { success: false, error };
		}
	}

	execQuery(databaseQuery, params) {
		return new Promise((data) => {
			connection.query(databaseQuery, params, function (error, result) {
				try {
					if (error) throw error;
					data({ success: true, result });
				} catch (error) {
					data({ success: false, error });
				}
			});
		});
	}
};
