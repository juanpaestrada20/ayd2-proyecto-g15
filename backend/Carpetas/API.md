# Crear carpeta

## Método

-   POST

## Ruta

-   34.136.7.97:3006/carpetas/crear

## Ejemplo Request

```json
{
	"ruta_actual": "root/",
	"nueva": "nueva_carpeta",
	"id_usuario": 26
}
```

## Posibles Respuestas

```json
{
	"status": true,
	"message": "Carpeta creada en: 'root/nueva_carpeta'."
}
```

```json
{
	"status": false,
	"message": "La carpeta 'root' no fue encontrada."
}
```

```json
{
	"status": false,
	"message": "La carpeta 'nueva_carpeta' ya existe en la ruta 'root/'."
}
```

```json
{
	"status": false,
	"message": "No se encontró el usuario especificado."
}
```

# Obtener carpeta root

## Método

-   GET

## Ruta

-   34.136.7.97:3006/carpetas/obtener/:id_usuario

## Ejemplo Request

-   34.136.7.97:3006/carpetas/obtener/1

## Posibles Respuestas

```json
{
    "status": true,
    "carpetas": [
        {
            "id_carpeta": 1,
            "nombre": "juanpa",
            "fecha_creacion": "2021-08-21T06:00:00.000Z",
            "cantidad_archivos": 5
        }
    ]
}
```

```json
{
	"status": false,
	"message": "No se encontró el usuario especificado."
}
```

# Obtener las carpetas dentro de una carpeta

## Método

-   GET

## Ruta

-   34.136.7.97:3006/carpetas/obtener/:id_usuario/:id_carpeta

## Ejemplo Request

-   34.136.7.97:3006/carpetas/obtener/1/12

## Posibles Respuestas

```json
{
    "status": true,
    "carpetas": [
        {
            "id_carpeta": 12,
            "nombre": "prueba2",
            "fecha_creacion": "2021-08-23T06:00:00.000Z",
            "cantidad_archivos": 0
        },
        {
            "id_carpeta": 14,
            "nombre": "prueba3",
            "fecha_creacion": "2021-08-23T06:00:00.000Z",
            "cantidad_archivos": 0
        }
    ]
}
```

```json
{
	"status": false,
	"message": "No se ha encontrado la carpeta especificada."
}
```

```json
{
	"status": false,
	"message": "No se encontró el usuario especificado."
}
```

# Actualizar nombre de una carpeta

## Método

-   PUT

## Ruta

-   34.136.7.97:3006/actualizar

## Ejemplo Request

```json
{
	"ruta_actual": "prueba/",
	"carpeta": "prueba2",
	"nombre_nuevo": "nueva_carpeta",
	"id_usuario": 26
}
```

## Posibles Respuestas

```json
{
	"status": true,
	"message": "Se ha modificado el nombre de la carpeta 'prueba2' a 'nueva_carpeta' de la ruta 'prueba/'."
}
```

```json
{
	"status": false,
	"message": "No se encontró la carpeta 'prueba2' en la ruta 'prueba/'."
}
```

```json
{
	"status": false,
	"message": "La carpeta 'nueva_carpeta' ya existe en la ruta 'prueba/'."
}
```

```json
{
	"status": false,
	"message": "No se encontró el usuario especificado."
}
```

# Eliminar carpeta

## Método

-   DELETE

## Ruta

-   34.136.7.97:3006/carpetas/eliminar/:id_usuario/:id_carpeta

## Ejemplo Request

-   34.136.7.97:3006/carpetas/eliminar/1/12

## Posibles Respuestas

```json
{
	"status": true,
	"message": "Se ha eliminado la carpeta 'nueva_carpeta'."
}
```

```json
{
	"status": false,
	"message": "No se ha encontrado la carpeta especificada."
}
```

```json
{
	"status": false,
	"message": "No se encontró el usuario especificado."
}
```
