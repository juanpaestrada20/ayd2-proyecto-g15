function wrap_function(anotherFunction, debugInfo = null) {
    return function (req, res) {
        if(debugInfo) console.log(debugInfo);
        anotherFunction(req, res).catch((err) => {
            return res.status(500).send({
            message: err["message"] ? err["message"] : "An error occurred.",
            });
        });
    };
  }
  module.exports = wrap_function;
  