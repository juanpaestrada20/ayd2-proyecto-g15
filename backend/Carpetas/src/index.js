const express = require("express");
const cors = require("cors");
const methods = require("./methods");
const env = require("./config.js");

const app = express();

const corsOptions = {
  origin: "*",
  optionsSuccessStatus: 200,
};

app.set("port", env.SERVER_PORT || 3006);

app.use(express.json());
app.use(cors(corsOptions));
app.use("/carpetas", methods);

app.listen(app.get("port"), () => {
  console.log("Servidor de carpetas en el puerto: ", app.get("port"));
});

module.exports = app;