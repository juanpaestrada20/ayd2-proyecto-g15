const mysql = require('mysql');
const env = require('./config.js');

const pool = mysql.createPool({
  host: env.MYSQL_HOST || 'localhost',
  user: env.MYSQL_USER || 'curioso',
  password: env.MYSQL_PASSWORD || 'curioso123',
  port: env.MYSQL_PORT || 3306,
  database: env.MYSQL_DB || 'ayd2_proyecto1',
  debug : false
});

function raw_query(q){
    return new Promise((resolve, reject) => {
          pool.query(q, (err, data)=>{
            if(err) reject(err);
            resolve(data);
          })
    });
}

module.exports.raw_query = raw_query;
