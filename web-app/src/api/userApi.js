import { handleResponse, handleError } from "./apiUtils";

const baseUrl = process.env.REACT_APP_MIDDLE;
const baseUrlUser = process.env.REACT_APP_USER;

export function saveUser(user, create) {
  console.log(user)
  return fetch(baseUrl + "/user", {
    method: create ? "POST" : "PUT",
    headers: { "content-type": "application/json" },
    body: JSON.stringify(user),
  })
    .then(handleResponse)
    .catch(handleError);
}

export function login(user) {
  return fetch(baseUrl + "/login", {
    method: "POST",
    headers: { "content-type": "application/json" },
    body: JSON.stringify(user),
  })
    .then(handleResponse)
    .catch(handleError);
}

export function validate(body) {
  return fetch(baseUrlUser + "/validate", {
    method: "POST",
    headers: { "content-type": "application/json" },
    body: JSON.stringify(body),
  })
    .then(handleResponse)
    .catch(handleError);
}

export function resend(body) {
  return fetch(baseUrlUser + "/resend", {
    method: "POST",
    headers: { "content-type": "application/json" },
    body: JSON.stringify(body),
  })
    .then(handleResponse)
    .catch(handleError);
}

