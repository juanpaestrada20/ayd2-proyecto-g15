import { handleError, handleResponse } from "./apiUtils";

const baseUrl = process.env.REACT_APP_MIDDLE;
const baseUrlBack = process.env.REACT_APP_BACKEND;

export function deleteFile(id_user, id_file) {
  return fetch(baseUrl + `/archivos/${id_user}/${id_file}`, {
    method: "DELETE",
    headers: { "content-type": "application/json" },
  })
    .then(handleResponse)
    .catch(handleError);
}

export function editFile(data) {
  return fetch(baseUrl + `/archivos/updateName`, {
    method: "PUT",
    headers: { "content-type": "application/json" },
    body: JSON.stringify(data),
  })
    .then(handleResponse)
    .catch(handleError);
}

export function createFile(data, user, folder) {
  return fetch(baseUrlBack + `/archivos/upload/${user}/${folder}`, {
    method: "POST",
    body: data,
  })
    .then(handleResponse)
    .catch(handleError);
}

export function moveFile(body) {
  console.log(body);
  return fetch(
    baseUrl +
    `/archivos/updateFile/${body.id_usuario}/${body.id_carpeta_nueva}/${body.id_archivo}`,
    {
      method: "POST",
      headers: { "Cntent-Type": "application/json" },
    }
  )
    .then(handleResponse)
    .catch(handleError);
}

export function moveToTrash(data) {
  return fetch(
    baseUrlBack +
    `/archivos/trash`,
    {
      method: "POST",
      body: JSON.stringify(data),
      headers: { "Content-Type": "application/json" },
    }
  )
    .then(handleResponse)
    .catch(handleError);
}

export function restoreFile(data) {
  return fetch(
    baseUrlBack +
    `/archivos/restore`,
    {
      method: "POST",
      body: JSON.stringify(data),
      headers: { "Content-Type": "application/json" },
    }
  )
    .then(handleResponse)
    .catch(handleError);
}

export function getFilesInTrash(id_usuario) {
  return fetch(
    baseUrlBack +
    `/archivos/trash/${id_usuario}`,
    {
      method: "GET",
      headers: { "Content-Type": "application/json" },
    }
  )
    .then(handleResponse)
    .catch(handleError);
}


export function sendFile(data) {
  return fetch(
    baseUrlBack +
    `/archivos/sendFile`,
    {
      method: "POST",
      body: JSON.stringify(data),
      headers: { "Content-Type": "application/json" },
    }
  )
    .then(handleResponse)
    .catch(handleError);
}