import { handleError, handleResponse } from "./apiUtils";

import { Factory } from '../models/factory'
import { types } from "../utils/types";

const baseUrl = process.env.REACT_APP_MIDDLE;

export async function getFolders(user, folder) {
  try {
    const rawFiles = await fetch(baseUrl + `/archivos/list/${folder}/1/100`, {
      method: "GET",
      headers: { "content-type": "application/json" },
    })
      .then(handleResponse)
      .catch(handleError);

    const rawFolders = await fetch(
      baseUrl + `/carpetas/obtener/${user}/${folder}`,
      {
        method: "GET",
        headers: { "content-type": "application/json" },
      }
    )
      .then(handleResponse)
      .catch(handleError);

    const factory = new Factory()
    const folders = rawFolders.carpetas.map(folder => factory.create(folder, types.FOLDER))
    const files = rawFiles.items.map(file => factory.create(file, types.FILE))
    return [...folders, ...files];
  } catch (error) {
    return [];
  }
}

export async function createFolder(user, folder, name) {
  const body = {
    ruta_actual: folder,
    nueva: name,
    id_usuario: user,
  };
  console.log(body);
  return fetch(baseUrl + "/carpetas/crear", {
    method: "POST",
    headers: { "content-type": "application/json" },
    body: JSON.stringify(body),
  })
    .then(handleResponse)
    .catch(handleError);
}

export function deleteFolder(id_user, id_folder) {
  return fetch(baseUrl + `/carpetas/eliminar/${id_user}/${id_folder}`, {
    method: "DELETE",
    headers: { "content-type": "application/json" },
  })
    .then(handleResponse)
    .catch(handleError);
}

export function editFolder(body) {
  return fetch(baseUrl + "/carpetas/actualizar", {
    method: "PUT",
    headers: { "content-type": "application/json" },
    body: JSON.stringify(body),
  })
    .then(handleResponse)
    .catch(handleError);
}

export async function getRoot(user) {
  return fetch(baseUrl + `/carpetas/obtener/${user}`, {
    method: "GET",
    headers: { "content-type": "application/json" },
  })
    .then(handleResponse)
    .catch(handleError);
}
