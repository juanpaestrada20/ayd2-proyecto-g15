import React, { useReducer, createContext } from "react";
import { useEffect } from "react";
import * as authActions from "./actionTypes/authActions";

const authReducer = (state, action) => {
  switch (action.type) {
    case authActions.LOGGED_IN_USER:
      return { ...state, user: action.payload };
    default:
      return state;
  }
};

const initialState = {
  user: null,
};

const localState = JSON.parse(localStorage.getItem("user"));

const AuthContext = createContext();

const AuthProvider = ({ children }) => {
  const [state, dispatch] = useReducer(authReducer, localState || initialState);
  useEffect(() => {
    localStorage.setItem("user", JSON.stringify(state));
  }, [state]);
  const value = { state, dispatch };
  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
};

export { AuthContext, AuthProvider };
