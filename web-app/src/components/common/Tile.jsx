import React from 'react'
import styles from './Tile.module.css'

export default function Tile({ title, content, icon: Icon = null }) {
    return (
        <div className={styles.tile}>
            <h6>{title}</h6>
            {Icon || <h6 className={styles.content} data-tooltip-location="left" data-tooltip={content}>{content.substring(0, 10)}</h6>}
        </div>
    )
}
