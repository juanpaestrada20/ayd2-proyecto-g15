import { useContext } from "react";
import { Navbar, Container, Nav } from "react-bootstrap"
import logo from '../../assets/images/logo.png'
import { AuthContext } from "../../context/authContext";
import * as authActions from "../../context/actionTypes/authActions";
import { Link } from "react-router-dom";



export default function Header() {
    const { state, dispatch } = useContext(AuthContext);
    const { user } = state;

    const logout = () => {
        localStorage.removeItem('user')
        dispatch({
            type: authActions.LOGGED_IN_USER,
            payload: null
        })
    }

    return (
        <Navbar className="navbar-background">
            <Container fluid>
                <Link to="/archivos" className="navbar-brand">
                    <img src={logo} className='navbar-logo' alt="logo" />KemoFiles
                </Link>
                <Nav>
                    {user ?
                        (<>
                            <Link to="/perfil" className="navbar-link">Perfil</Link>
                            <Link to="/" onClick={logout} className="navbar-link">Logout</Link>
                        </>) :
                        (<>
                            <Link to="/" className="navbar-link">Login</Link>
                            <Link to="/registro" className="navbar-link">Registro</Link>
                        </>)
                    }
                </Nav>
            </Container>
        </Navbar>
    )
}