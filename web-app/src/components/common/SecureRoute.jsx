import React, { useContext } from "react";
import { Redirect, Route } from "react-router-dom";
import { AuthContext } from "../../context/authContext";


export default function SecureRoute({ component: Component, scopes, ...rest }) {
    const { state } = useContext(AuthContext);
    const { user } = state;
    return (
        <Route
            {...rest}
            render={(props) =>
                !user ? <Redirect to="/" /> : <Component {...props} />
            }
        />
    );
}