import React from "react";
import ReusableInput from "../common/ReusableInput";
import '../login/LoginTUNEADO.css'

const CourseForm = ({
    user,
    errors = {},
    onChange,
    onSave,
    isDisabled = false,
}) => {
    return (

        <div style={{ height: '100vh' }} className="d-flex justify-content-center align-items-center">
            <div className="login-container d-flex flex-column justify-content-center align-items-center">
                <p className="login-title">Bienvenido</p>
                <form onSubmit={onSave}>
                    <ReusableInput
                        name="nickname"
                        label="Usuario"
                        value={user.nickname}
                        onChange={onChange}
                        error={errors.nickname}
                    />
                    <ReusableInput
                        type="password"
                        name="contra"
                        label="Contraseña"
                        value={user.contra}
                        onChange={onChange}
                        error={errors.contra}
                    />
                    <ReusableInput
                        type="password"
                        name="validar_contra"
                        label="Confirmar contraseña"
                        value={user.validar_contra}
                        onChange={onChange}
                        error={errors.validar_contra}
                    />
                    <ReusableInput
                        type="email"
                        name="correo"
                        label="Correo"
                        value={user.correo}
                        onChange={onChange}
                        error={errors.correo}
                    />
                    <ReusableInput
                        type="date"
                        name="fecha_nacimiento"
                        label="Fecha de nacimiento"
                        value={user.fecha_nacimiento}
                        onChange={onChange}
                        error={errors.fecha_nacimiento}
                    />

                    <ReusableInput
                        type="tel"
                        name="telefono"
                        label="Telefono"
                        value={user.telefono}
                        onChange={onChange}
                        error={errors.telefono}
                    />
                    <button className="login-button" type="submit" disabled={isDisabled}>
                        Registrar
                    </button>
                </form>
            </div>
        </div>
    );
};

export default CourseForm;
