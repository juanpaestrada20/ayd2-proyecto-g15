import React, { useState } from "react";
import RegistroForm from "./RegistroForm";
import { toast } from "react-toastify";
import * as userApi from "../../api/userApi";
import { formatDate } from '../../utils/utils'
import Cryptr from 'cryptr';
import md5 from 'md5';



export default function ManageRegistroPage({
    history,
    ...props
}) {
    const [user, setUser] = useState(
        { nickname: "", correo: "", contra: "", validar_contra: "", fecha_nacimiento: "2000-01-01", tipo: 0, phone: "" });
    const [errors, setErrors] = useState({});
    const [saving, setSaving] = useState(false);

    function handleChange(event) {
        const { name, value } = event.target;
        setUser((prevUser) => ({
            ...prevUser,
            [name]: value,
        }))
    }

    function formIsValid() {
        const { nickname, correo, contra, validar_contra, fecha_nacimiento, telefono } = user;
        const errors = {};

        if (!nickname) errors.nickname = "Este campo es obligatorio";
        if (!contra) errors.contra = "Este campo es obligatorio";
        if (!validar_contra) errors.validar_contra = "Este campo es obligatorio";
        if (!correo) errors.correo = "Este campo es obligatorio";
        if (!fecha_nacimiento) errors.fecha_nacimiento = "Este campo es obligatorio";
        if (!telefono) errors.fecha_nacimiento = "Este campo es obligatorio";
        if ((contra && validar_contra) && (contra !== validar_contra)) errors.validar_contra = "Las contraseñas no coinciden"
        setErrors(errors);

        return Object.keys(errors).length === 0;
    }

    function handleSave(event) {
        event.preventDefault();
        if (!formIsValid()) return;
        setSaving(true);
        const cryptr = new Cryptr("p")
        const data = { ...user, contra: md5(user.contra), correo: cryptr.encrypt(user.correo), validar_contra: md5(user.contra), fecha_nacimiento: formatDate(user.fecha_nacimiento), phone: user.telefono }
        userApi.saveUser(data, true)
            .then((result) => {
                if (result.error) {
                    toast.error(result.error)
                    setSaving(false);
                    return;
                }
                toast.success(result.msg, {
                    onClose: () =>
                        history.push("/")
                });
            })
            .catch((error) => {
                toast.error(error.message)
                setSaving(false);
            });
    }

    return (
        <>
            <RegistroForm
                user={user}
                errors={errors}
                onChange={handleChange}
                onSave={handleSave}
                isDisabled={saving}
            />
        </>
    )
}