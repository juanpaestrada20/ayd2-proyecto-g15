import React, { useContext } from 'react'
import { Button, Col, Modal } from "react-bootstrap";
import styles from './Acciones.module.css';
import { FaFileUpload, FaFolderPlus } from 'react-icons/fa';
import { useState } from 'react';
import ReusableInput from '../common/ReusableInput';
import { useDropzone } from 'react-dropzone'
import styled from 'styled-components';
import { createFile } from '../../api/fileApi';
import { AuthContext } from '../../context/authContext';
import { toast } from "react-toastify";

const getColor = (props) => {
    if (props.isDragAccept) {
        return '#00e676';
    }
    if (props.isDragReject) {
        return '#ff1744';
    }
    if (props.isDragActive) {
        return '#2196f3';
    }
    return '#eeeeee';
}

const Container = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 20px;
  border-width: 2px;
  border-radius: 2px;
  border-color: ${props => getColor(props)};
  border-style: dashed;
  background-color: #fafafa;
  color: #bdbdbd;
  outline: none;
  transition: border .24s ease-in-out;
`;


export default function Acciones({ fetchFiles, createFolder, id_carpeta }) {

    const [show, setShow] = useState({ isVisible: false, isFile: false });
    const handleShow = () => {
        //console.log('handleShow', show.isVisible)
        setShow({ ...show, isVisible: !show.isVisible })
    }

    const handleContent = (isFile) => {
        //console.log('handleContent', show, show.isVisible)
        setShow({ isVisible: true, isFile })
    }



    return (
        <>
            <Col className={styles.botonera}>
                <Button className={styles.boton} variant="light" ><FaFileUpload className={styles.icon} onClick={() => handleContent(true)} /></Button>

                <Button name="selenium-button-folder" className={styles.boton} variant="light" onClick={() => handleContent(false)}><FaFolderPlus className={styles.icon} /></Button>
            </Col>
            <CustomModal
                show={show.isVisible}
                handleShow={handleShow}
                title={show.isFile ? 'Subir archivos' : 'Carpeta nueva'}
                component={() => show.isFile ? FileForm({ id_carpeta, fetchFiles }) : FolderForm({ createFolder })}
            />
        </>
    )
}

function FolderForm({ createFolder }) {

    const [folder, setFolder] = useState("")

    const handleChange = (event) => {
        const { value } = event.target;
        setFolder(value);
    }


    return (
        <>
            <ReusableInput
                label="Nombre"
                value={folder}
                onChange={handleChange}
            />
            <div className={styles.createContainer}>
                <Button variant="primary" onClick={() => createFolder(folder)} >
                    Crear
                </Button>
            </div>
        </>)
}

function FileForm({ id_carpeta, fetchFiles }) {

    const { state } = useContext(AuthContext);
    const { user } = state;
    const {
        acceptedFiles,
        getRootProps,
        getInputProps,
        isDragActive,
        isDragAccept,
        isDragReject
    } = useDropzone({ accept: '' });


    const acceptedFileItems = acceptedFiles.map(file =>
    (<li key={file.path}>
        {file.path} - {file.size} bytes
    </li>)
    );


    const handleSubmit = () => {
        const fd = new FormData()
        console.log(acceptedFiles)
        acceptedFiles.map(f => {
            fd.append('file', f)
        })
        createFile(fd, user.id_usuario, id_carpeta).then((response) => {
            const { unsavedFiles = [] } = response;
            if (unsavedFiles.length !== 0) return toast.error('Algo salió mal...')
            toast.success('Carga exitosa')
            fetchFiles()
        }).catch(error => toast.error('Algo salió mal...'))
    }
    return (
        <>
            <Container {...getRootProps({ isDragActive, isDragAccept, isDragReject })}>
                <input {...getInputProps()} />
                <p>Drag 'n' drop some files here, or click to select files</p>
            </Container>
            {/*<input type="file" onChange={(e) => setFile(e.target.files)} multiple />*/}
            <aside>
                <ul>{acceptedFileItems}</ul>
            </aside>

            <div className={styles.createContainer}>
                <Button variant="primary" onClick={handleSubmit} >
                    Subir
                </Button>
            </div>
        </>)
}

function CustomModal({ title, show, handleShow, handleSubmit, component: Component }) {

    return <Modal centered
        show={show} onHide={handleShow}>
        <Modal.Header closeButton onClick={handleShow}>
            <Modal.Title>{title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Component handleShow={handleShow} />
        </Modal.Body>
    </Modal>
}

