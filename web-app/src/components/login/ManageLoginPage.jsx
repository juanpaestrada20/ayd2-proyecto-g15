import React, { useContext, useState } from "react";
import { CourseForm, ModalToken } from "./LoginForm";
import { toast } from "react-toastify";
import * as userApi from "../../api/userApi";
import { AuthContext } from "../../context/authContext";
import * as authActions from "../../context/actionTypes/authActions";
import { formatDate } from '../../utils/utils'
import md5 from "md5";

export default function ManageLoginPage({
    history,
    ...props
}) {

    const { dispatch } = useContext(AuthContext);

    const [user, setUser] = useState({ nickname: "", contra: "" });
    const [errors, setErrors] = useState({});
    const [saving, setSaving] = useState(false);
    const [show, setShow] = useState(false)
    const [session, setSession] = useState({})

    const [validate, setValidate] = useState({
        email: false,
        phone: false
    })

    function handleChange(event) {
        const { name, value } = event.target;
        setUser((prevUser) => ({
            ...prevUser,
            [name]: value,
        }));
    }

    function formIsValid() {
        const { nickname, contra } = user;
        const errors = {};

        if (!nickname) errors.nickname = "Este campo es obligatorio";
        if (!contra) errors.contra = "Este campo es obligatorio";
        setErrors(errors);

        return Object.keys(errors).length === 0;
    }

    function handleSave(event) {
        event.preventDefault();
        if (!formIsValid()) return;
        setSaving(true);
        const data = { ...user, contra: md5(user.contra) }
        userApi.login(data)
            .then((response) => {
                const { msg } = response;
                if (msg) {
                    toast.error(msg)
                    setSaving(false);
                    return;
                }


                const s = { ...response.user, contra: user.contra, fecha_nacimiento: formatDate(response.user.fecha_nacimiento) }
                setSession(s)
                if (response.user.auth_email == 1) {
                    setValidate({ phone: false, email: true })
                } if (response.user.auth_phone == 1) {
                    setValidate({ email: false, phone: true })
                }

                if ((response.user.auth_phone == 1) && (response.user.auth_email == 1)) {
                    setValidate({ email: true, phone: true })
                }

                if (response.user.auth_email != 1 && response.user.auth_phone != 1) {
                    handleLogin(s)
                } else {
                    setShow(true)
                }
            })
            .catch((error) => {
                toast.error(error.message)
                setSaving(false);
            });
    }

    const handleToken = (tokenCorreo, tokenTelefono) => {
        const body = {
            id_usuario: session.id_usuario,
            token_email: tokenCorreo,
            token_phone: tokenTelefono

        }
        userApi.validate(body).then((res) => {
            if (!res.success) {
                toast.error(res.msg)
                return
            }
            handleLogin()
        }).catch((error) => {
            toast.error(error.message)
            setSaving(false);
        });
    }

    const handleLogin = (s = session) => {
        localStorage.setItem('user', JSON.stringify(s))
        dispatch({
            type: authActions.LOGGED_IN_USER,
            payload: s
        })
        history.push("/archivos")
    }

    const reenviarToken = () => {
        const body = {
            id_usuario: session.id_usuario,
            auth_email: validate.email ? 1 : 0,
            auth_phone: validate.phone ? 1 : 0
        }
        userApi.resend(body).then((res) => {
            if (!res.success) {
                toast.error('No se pudo reenviar')
                return
            }
            toast.success('Enviado con exito')
        }).catch((error) => {
            toast.error(error.message)
            setSaving(false);
        });
    }

    return (
        <>
            <CourseForm
                user={user}
                errors={errors}
                onChange={handleChange}
                onSave={handleSave}
                isDisabled={saving}

            />
            <ModalToken
                show={show}
                setShow={setShow}
                handleToken={handleToken}
                validate={validate}
                reenviarToken={reenviarToken}
            />
        </>
    )
}