import React, { useState } from "react";
import ReusableInput from "../common/ReusableInput";
import { Modal, Button } from 'react-bootstrap'

export const CourseForm = ({
    user,
    errors = {},
    onChange,
    onSave,
    isDisabled = false,
}) => {
    return (
        <div style={{ height: '100vh' }} className="d-flex justify-content-center align-items-center">
            <div className="w-100 d-flex justify-content-center">
                <div className="login-container d-flex flex-column justify-content-center align-items-center">
                    <p className="login-title">Bienvenido</p>
                    <form onSubmit={onSave}>
                        <ReusableInput
                            name="nickname"
                            label="Usuario"
                            value={user.nickname}
                            onChange={onChange}
                            error={errors.nickname}
                        />
                        <ReusableInput
                            type="password"
                            name="contra"
                            label="Contraseña"
                            value={user.contra}
                            onChange={onChange}
                            error={errors.contra}
                        />
                        <button className="login-button" type="submit" disabled={isDisabled}>
                            Iniciar sesión
                        </button>
                    </form>
                </div>
            </div>

        </div>
    );
};

export const ModalToken = ({
    show,
    setShow,
    handleToken,
    validate,
    reenviarToken
}) => {

    const [tokenCorreo, setTokenCorreo] = useState('')
    const [tokenTelefono, setTokenTelefono] = useState('')
    return (
        <Modal
            show={show}
            size="md"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header >
                <Modal.Title id="contained-modal-title-vcenter">
                    Autenticacion
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {validate.email ?
                    <>
                        <h4>Ingrese el token de correo</h4>
                        <input onChange={(e) => setTokenCorreo(e.target.value)} className="w-100" name="tokeCorreo" />
                    </>
                    : <></>
                }
                {validate.phone ?
                    <>
                        <h4>Ingrese el token de telefono</h4>
                        <input onChange={(e) => setTokenTelefono(e.target.value)} className="w-100" name="tokenTel" />
                    </>
                    : <></>
                }
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={() => reenviarToken()}>Reenviar Token</Button>
                <Button onClick={() => handleToken(tokenCorreo, tokenTelefono)}>Siguiente</Button>
                <Button onClick={() => setShow(false)}>Cerrar</Button>
            </Modal.Footer>
        </Modal>
    )
}

