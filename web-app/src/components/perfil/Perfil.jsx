import React, { useContext, useEffect, useState } from 'react'
import ReusableInput from '../common/ReusableInput'
import styles from './Perfil.module.css'
import * as userApi from "../../api/userApi";
import { toast } from "react-toastify";
import { AuthContext } from '../../context/authContext';
import { formatDate } from '../../utils/utils'
import * as authActions from "../../context/actionTypes/authActions";
import Cryptr from 'cryptr';
import md5 from 'md5';
import { ButtonGroup, ToggleButton } from 'react-bootstrap';

export default function Perfil() {

    const { state, dispatch } = useContext(AuthContext);
    const { user: initialState } = state;
    const cryptr = new Cryptr("p");
    const [user, setUser] = useState(() => { return { ...initialState, correo: cryptr.decrypt(initialState.correo) } });
    const [errors, setErrors] = useState({});
    const [saving, setSaving] = useState(false);
    const [authValue, setAuthValue] = useState('0');

    useEffect(() => {
        setAuthValue(getAuth())
    }, [])

    const auths = [
        { name: 'Telefono', value: '1' },
        { name: 'Correo', value: '2' },
        { name: 'Ambos', value: '3' },
    ];

    const getAuth = () => {
        if (user.auth_email == 1 && user.auth_phone == 1) {
            return '3'
        } else if (user.auth_email == 1) {
            return '2'
        } else if (user.auth_phone == 1) {
            return '1'
        }
        return '0'

    }

    const onChange = (event) => {
        const { name, value } = event.target;
        setUser((prevUser) => ({
            ...prevUser,
            [name]: value,
        }))
    }

    const formIsValid = () => {
        const { nickname, correo, contra, fecha_nacimiento, phone } = user;
        const errors = {};

        if (!nickname) errors.nickname = "Este campo es obligatorio";
        if (!contra) errors.contra = "Este campo es obligatorio";
        if (!correo) errors.correo = "Este campo es obligatorio";
        if (!phone) errors.telefono = "Este campo es obligatorio";
        if (!fecha_nacimiento) errors.fecha_nacimiento = "Este campo es obligatorio";
        setErrors(errors);

        return Object.keys(errors).length === 0;
    }

    const handleSubmit = (event) => {
        event.preventDefault();
        if (!formIsValid()) return;
        setSaving(true);
        const auth_email = ((authValue == '2') || (authValue == '3')) ? 1 : 0
        const auth_phone = ((authValue == '1') || (authValue == '3')) ? 1 : 0

        const data = {
            ...user, contra: md5(user.contra), correo: cryptr.encrypt(user.correo), fecha_nacimiento: formatDate(user.fecha_nacimiento),
            auth_email: auth_email, auth_phone: auth_phone
        }
        userApi.saveUser(data, false)
            .then((result) => {
                if (!result.success) {
                    toast.error(result.msg)
                    setSaving(false);
                    return;
                }
                setSaving(true)
                localStorage.setItem('user', JSON.stringify(user))
                dispatch({
                    type: authActions.LOGGED_IN_USER,
                    payload: user
                })
                toast.success(result.msg);
            })
            .catch((error) => {
                toast.error(error.message)
                setSaving(false);
            });
    }

    return (
        <div style={{ height: '100vh' }} className="d-flex justify-content-center align-items-center">
            <div className={`${styles.loginContainer} d-flex flex-column justify-content-center align-items-center`}>
                <p className="login-title">Mis datos personales</p>
                <form onSubmit={handleSubmit} style={{ display: 'flex', flexDirection: 'column' }}>
                    <ReusableInput
                        disabled={true}
                        name="nickname"
                        label="Usuario"
                        value={user.nickname}
                        error={errors.nickname}
                    />
                    <ReusableInput
                        type="email"
                        name="correo"
                        label="Correo"
                        value={user.correo}
                        onChange={onChange}
                        error={errors.correo}
                    />
                    <ReusableInput
                        type="date"
                        name="fecha_nacimiento"
                        label="Fecha de nacimiento"
                        value={user.fecha_nacimiento}
                        onChange={onChange}
                        error={errors.fecha_nacimiento}
                    />
                    <ReusableInput
                        type="password"
                        name="contra"
                        label="Contraseña"
                        value={user.contra}
                        onChange={onChange}
                        error={errors.contra}
                    />

                    <ReusableInput
                        type="tel"
                        name="phone"
                        label="Telefono"
                        value={user.phone}
                        onChange={onChange}
                        error={errors.telefono}
                    />


                    <ButtonGroup style={{ paddingTop: 20 }}>
                        {auths.map((auth, idx) => (
                            <ToggleButton
                                key={idx}
                                id={`auth-${idx}`}
                                type="radio"
                                variant={'outline-primary'}
                                name="auth"
                                value={auth.value}
                                checked={authValue === auth.value}
                                onChange={(e) => {
                                    setAuthValue(e.currentTarget.value)

                                }}
                            >
                                {auth.name}
                            </ToggleButton>
                        ))}
                    </ButtonGroup>

                    <button className="login-button" type="submit" disabled={saving}>
                        Actualizar
                    </button>
                </form>
            </div>
        </div>
    )
}
