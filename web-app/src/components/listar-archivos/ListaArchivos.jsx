import React, { Component } from 'react';
import { Container, Table, Modal } from "react-bootstrap";
import { AuthContext } from '../../context/authContext';
import Archivo from './Archivo/Archivo';
import styles from './ListaArchivos.module.css';

export default class ListaArchivos extends Component {
    static contextType = AuthContext

    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        const { elements, history, fetchFiles, currentPath } = this.props
        return (
            <Container className={styles.container}>
                <Table>
                    <thead>
                        <tr>
                            <th>Tipo</th>
                            <th>Nombre</th>
                            <th></th>
                            <th>Fecha de Creacion</th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.elements.map((e, idx) => {
                            return (
                                <Archivo fetchFiles={fetchFiles} metadata={e} key={e.nombre} currentPath={currentPath}
                                    history={history} setContext={this.props.setContext} />
                            )
                        })}
                    </tbody>
                </Table>

            </Container>
        )
    }
}
