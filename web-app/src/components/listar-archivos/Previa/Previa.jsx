import React, { useState } from 'react'
import styles from './Previa.module.css'
import { MdContentCopy } from "react-icons/md";
import Block from './Block/Block';
import { Redirect } from "react-router-dom";

const API_URL = process.env.REACT_APP_BACKEND

export default function Previa(props) {
    const [err, setErr] = useState(false);

    const { metadata } = props.location;
    if (!metadata) {
        return <Redirect to="/archivos" />
    }

    const copyToClipboard = () => {
        const { nombre_almacenamiento } = metadata
        navigator.clipboard.writeText(`${API_URL}/archivos/download/${nombre_almacenamiento}`)
    }

    return (
        <div className={styles.container}>
            <div className={styles.preview}>

                {
                    err ?
                        <iframe
                            title="iframe" src={`${API_URL}/archivos/view/${metadata.nombre_almacenamiento}${metadata.extencion.includes('pdf') ? "#toolbar=0" : ""}`} style={{ width: "100%", height: '100%' }}></iframe>
                        :
                        <img onError={() => { setErr(true) }} src={`${API_URL}/archivos/view/${metadata.nombre_almacenamiento}`} alt="" />
                }
            </div>
            <div className={styles.metadata}>
                <div className={styles.content}>
                    <h4 className={styles.title}><a style={{ textDecoration: "none", color: "#cbcbcb" }} href={`${API_URL}/archivos/download/${metadata.nombre_almacenamiento}`}>Detalles</a></h4>
                    <Block title={"Información general"} tiles={[
                        { title: "Nombre", content: metadata.nombre },
                        { title: "Extensión", content: metadata.extencion },
                        { title: "Fecha de subida", content: new Date(metadata.fecha_subida).toLocaleDateString() },
                    ]} />
                    <div className={styles.separator} />
                    <Block title={"Uso compartido"} tiles={[
                        { title: "Obtener enlace", icon: < MdContentCopy className={styles.clipboard} onClick={copyToClipboard} /> }
                    ]} />
                </div>
            </div>
        </div>
    )
}
