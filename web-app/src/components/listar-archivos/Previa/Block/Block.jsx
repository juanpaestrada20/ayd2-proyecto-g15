import React from 'react'
import Tile from '../../../common/Tile'
import styles from './Block.module.css'

export default function Block({ title, tiles = [] }) {
    return (
        <div className={styles.section}>
            <h5 className={styles.title}>{title}</h5>
            {tiles.map(({ title, content, icon }) => {
                return <Tile title={title} content={content} icon={icon} />
            })}
        </div>
    )
}
