import React, { Component } from 'react';
import { Badge } from "react-bootstrap";
import { FaFolder, FaFileAlt } from 'react-icons/fa';
import { GoKebabVertical } from 'react-icons/go';
import { MdModeEdit, MdDelete, MdContentCopy, MdSend } from "react-icons/md";
import { BsX } from "react-icons/bs";
import { FaSave } from 'react-icons/fa'
import { Modal, Button } from 'react-bootstrap'

import styles from './Archivo.module.css';
import * as fileApi from "../../../api/fileApi";
import * as folderApi from "../../../api/folderApi";
import { AuthContext } from '../../../context/authContext';
import { toast } from 'react-toastify';

export default class Archivo extends Component {

    static contextType = AuthContext

    constructor(props) {
        super(props);
        this.state = {
            showMore: true,
            isEditing: false,
            name: this.props.metadata.nombre,
            ruta_actual: this.props.currentPath(),
            correo: '',
            showModal: false
        }
    }

    handleNavigation = (isFile) => {
        if (this.state.isEditing) return;
        const { history, metadata, setContext } = this.props;
        if (!isFile) setContext({ id: this.props.metadata.id_carpeta, name: this.props.metadata.nombre })
        else {
            history.push({
                pathname: '/previa',
                metadata
            })
        }
    }

    toggleShowMore = () => {
        this.setState({
            showMore: !this.state.showMore,
            isEditing: false
        })
    }

    toggleIsEditing = (isFile) => {
        const { isEditing, name: nombre_nuevo } = this.state;
        if (isEditing) {
            const { metadata, fetchFiles } = this.props
            if (isFile) {
                const { id_carpeta, id_archivo } = metadata
                const body = {
                    id_carpeta,
                    nombre_nuevo,
                    id_archivo
                }
                fileApi.editFile(body)
                    .then(response => {
                        if (!response.success) return toast.error(response.error)
                        toast.success(response.msg)
                        fetchFiles();
                    })
                    .catch((error) => {
                        toast.error(error)
                    });
            } else {
                const { id_usuario } = this.context.state.user;
                const { nombre: carpeta } = this.props.metadata
                const { ruta_actual } = this.state
                const body = {
                    ruta_actual,
                    carpeta,
                    nombre_nuevo,
                    id_usuario
                }

                console.log(body)
                folderApi.editFolder(body)
                    .then(response => {
                        if (!response.status) return toast.error(response.message)
                        toast.success(response.message)
                        fetchFiles();
                    })
                    .catch((error) => {
                        toast.error(error)
                    });
            }
        }
        this.setState({
            isEditing: !isEditing
        })
    }

    handleDelete = (isFile) => {
        const { user } = this.context.state;
        const { metadata, fetchFiles } = this.props
        if (isFile) {
            const data = { id_archivo: metadata.id_archivo, id_usuario: user.id_usuario }
            fileApi.moveToTrash(data).then(response => {
                if (!response.success) return toast.error(response.error)
                toast.success(response.msg)
                fetchFiles();
            })
                .catch((error) => {
                    console.log(error)
                });
        }
        else {
            folderApi.deleteFolder(user.id_usuario, metadata.id_carpeta).then(response => {
                fetchFiles();
            })
                .catch((error) => {
                    console.log(error)
                });
        }

    }

    handleOnChange = (e) => {
        this.setState({ ...this.state, name: e.target.value })
    }


    copyToClipboard = () => {
        const { id_archivo } = this.props.metadata
        navigator.clipboard.writeText(id_archivo)
    }

    sendFileEmail = () => {
        const { nombre_almacenamiento } = this.props.metadata
        const body = {
            email: this.state.correo,
            nombre_almacenamiento: nombre_almacenamiento
        }
        fileApi.sendFile(body).then(response => {
            if (!response.success) return toast.error("Error!")
            toast.success("Archivo enviado")
        })
            .catch((error) => {
                console.log(error)
            });

    }



    render() {
        const { metadata } = this.props;
        const { name, isEditing } = this.state
        const isFile = !!metadata.extencion
        return (
            <>
                <tr className={styles.element}>
                    <td>{isFile ? <FaFileAlt className={styles.icon} /> : <FaFolder className={styles.icon} />}</td>
                    <td onClick={() => this.handleNavigation(isFile)}><input name={`selenium-input-${name}`} type={"text"} value={name} onChange={this.handleOnChange} disabled={!isEditing} className={styles.fileInput} /></td>
                    <td>{isFile ? '' : <Badge bg="primary">{metadata.cantidad_archivos} archivos</Badge>}</td>
                    <td>{metadata.fecha_subida ? metadata.fecha_subida.slice(0, 10) : metadata.fecha_creacion.slice(0, 10)}</td>
                    <td>
                        {
                            this.state.showMore ?
                                <GoKebabVertical name={`selenium-show-${name}`} className={styles.icon} onClick={this.toggleShowMore} />
                                : (
                                    <>
                                        {isEditing ? <FaSave className={styles.iconop} onClick={() => this.toggleIsEditing(isFile)} /> :
                                            <MdModeEdit className={styles.iconop} onClick={() => this.toggleIsEditing(isFile)} />}
                                        <MdDelete name={`selenium-delete-${name}`} className={styles.iconop} onClick={() => this.handleDelete(isFile)} />
                                        {isFile ?
                                            <>
                                                < MdContentCopy onClick={this.copyToClipboard} className={`${styles.clipboard} ${styles.iconop}`} />
                                                <MdSend className={styles.iconop} onClick={() => this.setState({ ...this.state, showModal: true })} />
                                            </>
                                            :
                                            <></>
                                        }
                                        <BsX className={styles.iconop} onClick={this.toggleShowMore} />

                                    </>
                                )
                        }
                    </td>
                </tr>
                <Modal
                    show={this.state.showModal}
                    size="md"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered
                >
                    <Modal.Header >
                        <Modal.Title id="contained-modal-title-vcenter">
                            Enviar archivo
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <>
                            <h4>Ingrese el correo</h4>
                            <input onChange={(e) => this.setState({ ...this.state, correo: e.target.value })} className="w-100" type="email" name="tokeCorreo" />
                        </>

                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={this.sendFileEmail}>Enviar</Button>
                        <Button onClick={() => this.setState({ ...this.state, showModal: false })}>Cerrar</Button>
                    </Modal.Footer>
                </Modal>
            </>
        )
    }


}
