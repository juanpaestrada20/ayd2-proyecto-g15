import React, { useContext, useEffect, useState } from "react";
import { Container, Button, Col, Modal, ListGroup, Row, Spinner } from "react-bootstrap";
import { toast } from "react-toastify";
import { IoIosArrowBack } from "react-icons/io";
import { FaTrashAlt } from "react-icons/fa";
import { MdSettingsBackupRestore } from "react-icons/md";

import styles from "./Home.module.css";
import { AuthContext } from "../../context/authContext";
import Acciones from "../acciones-archivos/Acciones";
import ListaArchivos from "../listar-archivos/ListaArchivos";
import * as fileApi from "../../api/fileApi";
import * as folderApi from "../../api/folderApi";

const ROOT = { id: -1, name: "" };

export default function Home({ history }) {

  const { state } = useContext(AuthContext);
  const { user } = state;

  const [context, setContext] = useState([ROOT]);
  const [deletedItems, setDeletedItems] = useState([]);
  const [elements, setElements] = useState([]);
  const [spinner, setSpinner] = useState(false);
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(!show);

  const pushContext = (folder) => {
    setContext([...context, folder]);
  };

  const popContext = () => {
    setContext(context.slice(0, -1));
  };

  const addFolder = (name) => {
    const folder = getCurrentPath();
    folderApi.createFolder(user.id_usuario, folder, name)
      .then((response) => {
        if (!response.status) return toast.error(response.message);
        toast.success(response.message);
        fetchFiles();
      })
      .catch((error) => toast.error("Algo salió mal..."));
  };

  const getCurrentPath = () => {
    return context
      .map(function (elem) {
        return elem.name;
      })
      .join("/");
  };

  const restoreFile = (id_archivo) => {
    const data = {
      id_archivo,
      id_usuario: user.id_usuario,
    };
    fileApi
      .restoreFile(data)
      .then((response) => {
        if (!response.success) {
          return toast.error(response.error);
        }
        toast.success(response.msg);
        fetchFiles();
      })
      .catch((error) => toast.error("Algo salió mal..."));
  };

  const deletePermanently = (id_archivo) => {
    fileApi
      .deleteFile(user.id_usuario, id_archivo)
      .then((response) => {
        fetchFiles();
      })
      .catch((error) => toast.error("Algo salió mal..."));
  };

  const getDeletedFiles = () => {
    fileApi
      .getFilesInTrash(user.id_usuario)
      .then((response) => {
        if (response.success) {
          setDeletedItems(response.items);
        }
      })
      .catch((error) => console.log(error));
  };

  useEffect(() => {
    fetchFiles();
    window.addEventListener("paste", movefile);

    return () => window.removeEventListener("paste", movefile);
  }, [context]);

  const movefile = (e) => {
    const nombre = e.clipboardData.getData("text");
    console.log(context);
    if (!nombre) return;
    const body = {
      id_archivo: Number(nombre),
      id_carpeta_nueva: context[context.length - 1].id,
      id_usuario: user.id_usuario,
    };
    fileApi
      .moveFile(body)
      .then((response) => {
        if (!response.success) return toast.error(response.error);
        toast.success(response.msg);
        fetchFiles();
      })
      .catch((error) => {
        toast.error(error);
      });
  };

  useEffect(() => {
    folderApi.getRoot(user.id_usuario).then((res) => {
      if (res.carpetas.length === 0) return;
      setContext([
        {
          id: res.carpetas[0].id_carpeta,
          name: res.carpetas[0].nombre,
        },
      ]);
    });
  }, []);

  const fetchFiles = () => {
    setSpinner(true);
    getDeletedFiles();
    folderApi.getFolders(user.id_usuario, context[context.length - 1].id)
      .then((res) => {
        setElements(res);
      })
      .then(setSpinner(false));
  };

  return (
    <>
      {spinner ? (
        <div className="vh-100 d-flex justify-content-center align-items-center">
          <Spinner animation="border" role="status">
            <span className="visually-hidden"> Loading...</span>
          </Spinner>
        </div>
      ) : (
        <>
          <Modal centered show={show} onHide={handleClose}>
            <Modal.Header closeButton>
              <Modal.Title>Tus archivos en la papelera</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              {deletedItems.map(({ id_archivo, nombre }, idx) => (
                <ListGroup horizontal="sm" className="my-2" key={idx}>
                  <ListGroup.Item>
                    <MdSettingsBackupRestore
                      onClick={() => restoreFile(id_archivo)}
                      color="orange"
                    />
                  </ListGroup.Item>
                  <ListGroup.Item>
                    <FaTrashAlt
                      onClick={() => deletePermanently(id_archivo)}
                      color="red"
                    />
                  </ListGroup.Item>
                  <ListGroup.Item>{nombre}</ListGroup.Item>
                </ListGroup>
              ))}
            </Modal.Body>
          </Modal>
          <Container className={styles.main}>
            <Row>
              <Col md="2">
                {context.length !== 1 ? (
                  <Button className={styles.boton} onClick={popContext}>
                    <IoIosArrowBack />
                  </Button>
                ) : (
                  <></>
                )}
              </Col>
              <Acciones
                fetchFiles={fetchFiles}
                context={context}
                createFolder={addFolder}
                id_carpeta={context[context.length - 1]?.id}
              />
            </Row>
            <Row className={styles.lista}>
              <ListaArchivos
                fetchFiles={fetchFiles}
                context={context}
                setContext={pushContext}
                currentPath={getCurrentPath}
                history={history}
                elements={elements}
              />
            </Row>
            <div className={styles.circle}>
              <FaTrashAlt
                size={30}
                className={styles.fixedbutton}
                onClick={handleClose}
              />
            </div>
          </Container>
        </>
      )}
    </>
  );
}
