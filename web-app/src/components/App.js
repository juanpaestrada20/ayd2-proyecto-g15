import "bootstrap/dist/css/bootstrap.min.css";
import { Route, Switch } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "./App.css";
import Header from "./common/Header";
import PageNotFound from "./PageNotFound";
import ManageLoginPage from "./login/ManageLoginPage";
import ManageRegistroPage from "./registro/ManageRegistroPage";
import SecureRoute from "./common/SecureRoute";
import Previa from "./listar-archivos/Previa/Previa";
import Home from "./views/Home";
import Perfil from "./perfil/Perfil";

function App() {
  return (
    <>
      <Header />
      <Switch>
        <Route exact path="/" component={ManageLoginPage} />
        <SecureRoute exact path="/previa" component={Previa} />
        <SecureRoute exact path="/perfil" component={Perfil} />
        <Route exact path="/registro" component={ManageRegistroPage} />
        <SecureRoute exact path="/archivos" component={Home} />
        <Route component={PageNotFound} />
      </Switch>
      <ToastContainer autoClose={3000} hideProgressBar />
    </>
  );
}

export default App;
