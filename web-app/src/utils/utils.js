export function formatDate(date) {
  const items = date.split("-").reverse();
  return items.join("-");
}
