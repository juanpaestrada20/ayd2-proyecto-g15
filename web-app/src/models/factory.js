import { types } from '../utils/types';
import { File } from './file'
import { Folder } from './folder'

export class Factory {

    create(args, type) {
        switch (type) {
            case types.FILE:
                return new File(args);
            case types.FOLDER:
                return new Folder(args);
            default:
                throw new Error(`${type} not supported`)
        }
    }
}