import { Entity } from './entity'
export class Folder extends Entity {

    /*
        args comes with the following attributes:
        - id_carpeta
        - nombre
        - fecha_creacion
        - cantidad_archivos
    */

    constructor(args) {
        super();
        Object.assign(this, args);
    }
}