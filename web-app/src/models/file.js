import { Entity } from './entity'
export class File extends Entity {

    /*
        args comes with the following attributes:
        - id_archivo
        - nombre
        - nombre_almacenamiento
        - extencion
        - fecha_subida
        - size
        - id_carpeta
        - id_usuario
    */

    constructor(args) {
        super();
        Object.assign(this, args);
    }
}