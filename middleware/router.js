const express = require('express');
const router = express.Router();
const env = require('./config.js');
const Logs = require('./logs');
const logs = new Logs();

//-----------------URLS----------------------
const api_host = env.SERVICES_HOST || 'localhost';
const archivos = 'archivos';
const carpetas = 'carpetas';
const usuarios = 'user';
const url_carpetas = `${api_host}:3006/${carpetas}`;
const url_archivos = `${api_host}:3005/${archivos}`;
const url_usuarios_actualizar = `${api_host}:3003/${usuarios}`;
const url_usuarios_crear = `${api_host}:3002/${usuarios}`;
const url_usuarios_eliminar = `${api_host}:3001/${usuarios}`;
const url_login = `${api_host}:3000`;


//--------------RUTA PRUEBA------------------
router.get(`/`, async (req,res) => {
	res.json({success:1})
});

//--------------ARCHIVOS----------------------

router.post(`/${archivos}/upload/:id_usuario/:id_carpeta`, async (req, res) => {
	const url = `${url_archivos}/upload/${req.params.id_usuario}/${req.params.id_carpeta}`;
	const entrada = `params:[(id_usuario:${req.params.id_usuario}),(id_carpeta:${req.params.id_carpeta})]`;
	const metodo = 'POST';
	const response = await logs.makeRequest(null,url,metodo,req.file);
	let es_error = true
	const salida = JSON.stringify(response);
	if(response.unsavedFiles.length()===0){
		es_error = false
	}
	await logs.createLog(url, entrada, salida, metodo, es_error);
	res.json(response)
});

router.delete(`/${archivos}/:id_usuario/:id_archivo`, async (req, res) => {
	const url = `${url_archivos}/${req.params.id_usuario}/${req.params.id_archivo}`;
	const entrada = `params:[(id_usuario,${req.params.id_usuario}),(id_archivo:${req.params.id_archivo})]`;
	const metodo = 'DELETE';
	const response = await logs.makeRequest(null,url,metodo);
	const salida = JSON.stringify(response);
	const es_error = !response.status || true
	await logs.createLog(url, entrada, salida, metodo, es_error);
	res.json(response)
});

router.get(`/${archivos}/view/:nombre_almacenamiento`, async (req, res) => {
	const url = `${url_archivos}/view/${req.params.nombre_almacenamiento}`;
	const entrada = `params:[(nombre_almacenamiento:${req.params.nombre_almacenamiento})]`;
	const metodo = 'GET';
	const response = await logs.makeRequest(null,url,metodo);
	const salida = JSON.stringify(response);
	const es_error = !response.status || true
	await logs.createLog(url, entrada, salida, metodo, es_error);
	res.json(response)
});

router.get(`/${archivos}/download/:nombre_almacenamiento`, async (req, res) => {
	const url = `${url_archivos}/download/${req.params.nombre_almacenamiento}`;
	const entrada = `params:[(nombre_almacenamiento:${req.params.nombre_almacenamiento})]`;
	const metodo = 'GET';
	const response = await logs.makeRequest(null,url,metodo);
	const salida = JSON.stringify(response)
	const es_error = !response.status || true
	await logs.createLog(url, entrada, salida, metodo, es_error);
	res.json(response)
});

router.get(`/${archivos}/list/:id_carpeta/:page/:items`, async (req, res) => {
	const url = `${url_archivos}/list/${req.params.id_carpeta}/${req.params.page}/${req.params.items}`;
	const entrada = `params:[(id_carpeta:${req.params.id_carpeta}),(page:${req.params.page}),(items:${req.params.items})]`;
	const metodo = 'GET';
	const response = await logs.makeRequest(null,url,metodo);
	const salida = JSON.stringify(response)
	const es_error = !response.status || true
	await logs.createLog(url, entrada, salida, metodo, es_error);
	res.json(response)
});

router.put(`/${archivos}/updateName`, async (req, res) => {
	const url = `${url_archivos}/updateName`;
	const entrada = JSON.stringify(req.body);
	const metodo = 'PUT';
	const response = await logs.makeRequest(entrada,url,metodo);
	const salida = JSON.stringify(response)
	const es_error = !response.status || true
	await logs.createLog(url, entrada, salida, metodo, es_error);
	res.json(response)
});

router.post(`/${archivos}/updateFile/:id_usuario/:id_carpeta/:id_archivo`, async (req, res) => {
	const url = `${url_archivos}/updateFile/${req.params.id_usuario}/${req.params.id_carpeta}/${req.params.id_archivo}`;
	const entrada = `params:[(id_usuario:${req.params.id_usuario}),(id_carpeta:${req.params.id_carpeta}),(id_archivo:${req.params.id_archivo})]`;
	const metodo = 'POST';
	const response = await logs.makeRequest(null,url,metodo);
	const salida = JSON.stringify(response)
	const es_error = !response.status || true
	await logs.createLog(url, entrada, salida, metodo, es_error);
	res.json(response)
});

router.post(`/${archivos}/trash`, async (req, res) => {
	const url = `${url_archivos}/trash`;
	const entrada = JSON.stringify(req.body);
	const metodo = 'POST';
	const response = await logs.makeRequest(entrada,url,metodo);
	const salida = JSON.stringify(response)
	const es_error = !response.status || true
	await logs.createLog(url, entrada, salida, metodo, es_error);
	res.json(response)
});

router.post(`/${archivos}/restore`, async (req, res) => {
	const url = `${url_archivos}/restore`;
	const entrada = JSON.stringify(req.body);
	const metodo = 'POST';
	const response = await logs.makeRequest(entrada,url,metodo);
	const salida = JSON.stringify(response)
	const es_error = !response.status || true
	await logs.createLog(url, entrada, salida, metodo, es_error);
	res.json(response)
});

router.get(`/${archivos}/trash/:id_usuario`, async (req, res) =>{
	const url = `${url_archivos}/${req.params.id_usuario}`;
	const entrada = `params:[(id_usuario,${req.params.id_usuario})]`;
	const metodo = 'GET';
	const response = await logs.makeRequest(null,url,metodo);
	const salida = JSON.stringify(response);
	const es_error = !response.status || true
	await logs.createLog(url, entrada, salida, metodo, es_error);
	res.json(response)
});

//---------------------------CARPETAS------------------------

router.post(`/${carpetas}/crear`, async (req, res) => {
	const url = `${url_carpetas}/crear`;
	const entrada = JSON.stringify(req.body);
	const metodo = 'POST';
	const response = await logs.makeRequest(entrada,url,metodo);
	const salida = JSON.stringify(response)
	const es_error = !response.status || true
	await logs.createLog(url, entrada, salida, metodo, es_error);
	res.json(response)
});

router.get(`/${carpetas}/obtener/:id_usuario`, async (req, res) => {
	const url = `${url_carpetas}/obtener/${req.params.id_usuario}`;
	const entrada = `params:[(id_usuario:${req.params.id_usuario})]`;
	const metodo = 'GET';
	const response = await logs.makeRequest(null,url,metodo);
	const salida = JSON.stringify(response)
	const es_error = !response.status || true
	await logs.createLog(url, entrada, salida, metodo, es_error);
	res.json(response)
});

router.get(`/${carpetas}/obtener/:id_usuario/:id_carpeta`, async (req, res) => {
	const url = `${url_carpetas}/obtener/${req.params.id_usuario}/${req.params.id_carpeta}`;
	const entrada = `params:[(id_usuario:${req.params.id_usuario}),(id_carpeta:${req.params.id_carpeta})]`;
	const metodo = 'GET';
	const response = await logs.makeRequest(null,url,metodo);
	const salida = JSON.stringify(response)
	const es_error = !response.status || true
	await logs.createLog(url, entrada, salida, metodo, es_error);
	res.json(response)
});

router.put(`/${carpetas}/actualizar`, async (req, res) => {
	const url = `${url_carpetas}/actualizar`;
	const entrada = JSON.stringify(req.body);
	const metodo = 'PUT';
	const response = await logs.makeRequest(entrada,url,metodo);
	const salida = JSON.stringify(response)
	const es_error = !response.status || true
	await logs.createLog(url, entrada, salida, metodo, es_error);
	res.json(response)
});

router.delete(`/${carpetas}/eliminar/:id_usuario/:id_carpeta`, async (req, res) => {
	const url = `${url_carpetas}/eliminar/${req.params.id_usuario}/${req.params.id_carpeta}`;
	const entrada = `params:[(id_usuario:${req.params.id_usuario}),(id_carpeta:${req.params.id_carpeta})]`;
	const metodo = 'DELETE';
	const response = await logs.makeRequest(null,url,metodo);
	const salida = JSON.stringify(response)
	const es_error = !response.status || true
	await logs.createLog(url, entrada, salida, metodo, es_error);
	res.json(response)
});





//---------------------------USUARIOS------------------------

router.put(`/${usuarios}`, async (req, res) => {
	const url = `${url_usuarios_actualizar}`;
	const entrada = JSON.stringify(req.body);
	const metodo = 'PUT';
	const response = await logs.makeRequest(entrada,url,metodo);
	const salida = JSON.stringify(response)
	const es_error = !response.success || true
	await logs.createLog(url, entrada, salida, metodo, es_error);
	res.json(response)
});

router.put(`/${usuarios}/contra`, async (req, res) => {
	const url = `${url_usuarios_actualizar}/contra`;
	const entrada = JSON.stringify(req.body);
	const metodo = 'PUT';
	const response = await logs.makeRequest(entrada,url,metodo);
	const salida = JSON.stringify(response)
	const es_error = !response.success || true
	await logs.createLog(url, entrada, salida, metodo, es_error);
	res.json(response)
});

router.post(`/${usuarios}`,async (req, res) => {
	const url = `${url_usuarios_crear}`;
	const entrada = JSON.stringify(req.body);
	const metodo = 'POST';
	const response = await logs.makeRequest(entrada,url,metodo);
	const salida = JSON.stringify(response)
	const es_error = !response.success || true
	await logs.createLog(url, entrada, salida, metodo, es_error);
	res.json(response)
});

router.delete(`/${usuarios}`, async (req, res) => {
	const url = `${url_usuarios_eliminar}`;
	const entrada = JSON.stringify(req.body);
	const metodo = 'DELETE';
	const response = await logs.makeRequest(entrada,url,metodo);
	const salida = JSON.stringify(response)
	const es_error = !response.success || true
	await logs.createLog(url, entrada, salida, metodo, es_error);
	res.json(response)
});

router.post(`/login`, async (req, res) => {
	const url = `${url_login}/login`;
	const entrada = JSON.stringify(req.body);
	const metodo = 'POST';
	const response = await logs.makeRequest(entrada,url,metodo);
	const salida = JSON.stringify(response)
	const es_error = !response.success || true
	await logs.createLog(url, entrada, salida, metodo, es_error);
	res.json(response)
});

router.post(`/validate`, async (req, res) => {
	const url = `${url_login}/validate`;
	const entrada = JSON.stringify(req.body);
	const metodo = 'POST';
	const response = await logs.makeRequest(entrada,url,metodo);
	const salida = JSON.stringify(response)
	const es_error = !response.success || true
	await logs.createLog(url, entrada, salida, metodo, es_error);
	res.json(response)
});

router.post(`/resend`, async (req, res) => {
	const url = `${url_login}/resend`;
	const entrada = JSON.stringify(req.body);
	const metodo = 'POST';
	const response = await logs.makeRequest(entrada,url,metodo);
	const salida = JSON.stringify(response)
	const es_error = !response.success || true
	await logs.createLog(url, entrada, salida, metodo, es_error);
	res.json(response)
});

module.exports = router;
