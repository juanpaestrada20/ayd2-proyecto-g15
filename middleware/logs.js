const connection = require('./mysql.js');
const axios = require('axios');


module.exports = class LogsHandler {

	async makeRequest(data, url, method, files = null){

		const config = {
			method: method,
    		url: url,
    		headers: { "content-type": "application/json" },
			data: data,
			files: files
		}
		let res = await axios(config);
		return(res.data)

	}

    async createLog(ruta, entrada, salida, metodo, es_error){
		const query = `INSERT INTO Log(ruta, entrada, salida, metodo, es_error) VALUES (?, ?, ?, ?, ?)`;
		const params = [ruta, entrada, salida, metodo, es_error];
		try {
			await this.execQuery(query, params);
		} catch (err) {
			console.log(err)
		}
    }

    execQuery(databaseQuery, params){
		return new Promise((data) => {
			connection.query(databaseQuery, params, function (error, result) {
				try {
					if (error) throw error;
					data({ success: true, result });
				} catch (error) {
					data({ success: false, error });
				}
			});
		});
	}

}