echo $COMMIT
echo COMMIT=$COMMIT >> .env.prod
cp /home/config/backend/.env ./backend2/.env
cp ./backend2/test/Dockerfile ./backend2/Dockerfile
cp /home/config/frontend/.env ./web-app/.env
cp /home/config/front-test/.env ./web-test/.env

# Correr pruebas de backend CALIFICACION
sudo docker build -t backendtest ./backend2 --no-cache
sudo docker run --name backend-test-container  backendtest

#Correr pruebas de frontend
sudo docker build -t fronttest ./web-test --no-cache
sudo docker run --name front-test-container  fronttest


#docker-compose -f docker-compose.test.yml up frontend 
cp ./backend2/prod/Dockerfile ./backend2/Dockerfile
output=$(sudo docker ps -a -q | xargs docker inspect -f '{{ .State.ExitCode }}' | grep -v '^0' | head -n 1 | tr -d ' ' ) ; [ -z $output ] && output=$(echo "0") 
[ $output = "0" ] && sudo docker build -t alesandrog/backend:$COMMIT  ./backend2 --no-cache && sudo docker push alesandrog/backend:$COMMIT && sudo docker build -t alesandrog/frontend:$COMMIT  ./web-app --no-cache && sudo docker push alesandrog/frontend:$COMMIT
#sudo docker build -t alesandrog/backend:$COMMIT backend2/prod --no-cache
#sudo docker push alesandrog/backend:$COMMIT
#sudo docker-compose -f docker-compose.test.yml down --rmi all
#sudo docker-compose -f docker-compose.yml down --rmi all
sudo docker rm backend-test-container -f 
sudo docker rmi backendtest alesandrog/backend:$COMMIT -f
sudo docker rm front-test-container -f 
sudo docker rmi fronttest alesandrog/frontend:$COMMIT -f
#sudo docker rmi  alesandrog/frontend:$COMMIT -f
exit $output



#backend-svc.default.svc.cluster.local


#REACT_APP_BACKEND=http://35.247.104.150:3000
#REACT_APP_MIDDLE=http://35.247.104.150:3000
#REACT_APP_USER=http://35.247.104.150:3000


