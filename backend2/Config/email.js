const nodemailer = require('nodemailer');
const env = require('./config.js');
const Cryptr = require('cryptr');

const transporter = nodemailer.createTransport({
	host: 'smtp.gmail.com',
	port: 465,
	secure: true,
	auth: {
		user: env.EMAIL,
		pass: env.EMAIL_PASS,
	},
});
/*  */

async function sendEmail(token, to_user) {
	const cryptr = new Cryptr('p');
	const email = cryptr.decrypt(to_user);

	const mail = `<p>Tu código de seguridad es: <b>${token}</b></p>`;
	const mailOptions = {
		from: `"Kemofiles" <${env.EMAIL}>`,
		to: email,
		subject: 'Codigo de seguridad de Kemofiles',
		html: mail,
	};
	try {
		await transporter.sendMail(mailOptions);
		return true;
	} catch (e) {
		console.log(e);
		return false;
	}
}

async function sendEmailAttachment(filename, nombre_almacenamiento, email) {

	const mail = `<p>Te enviamos tu archivo: ${filename}</p>`;
	const mailOptions = {
		from: `"Kemofiles" <${env.EMAIL}>`,
		to: email,
		subject: filename,
		html: mail,
		attachments:[
			{
				filename,
				path: `${__dirname.replace('/Config','')}/uploads/${nombre_almacenamiento}`
			}
		]
	};
	try {
		await transporter.sendMail(mailOptions);
		return true;
	} catch (e) {
		console.log(e);
		return false;
	}
}

module.exports = {sendEmail,sendEmailAttachment};
