const connection = require('../Config/mysql');

var db_instance = {
    getSingleton: (function() {
        var singleton;
        return function() {
            if (!singleton) {
                singleton = {
                    execQuery: function(databaseQuery, params) {
                        if(params){
                            return new Promise((data) => {
                                connection.query(databaseQuery, params, function (error, result) {
                                    try {
                                        if (error) throw error;
                                        data({ success: true, result });
                                    } catch (error) {
                                        data({ success: false, error });
                                    }
                                });
                            });
                        } else {
                            return new Promise((resolve, reject) => {
                                connection.query(databaseQuery, (err, data) => {
                                    if (err) reject(err);
                                    resolve(data);
                                });
                            });
                        }
                    }
                }
            }
            return singleton;
        };
    }())
}

module.exports = db_instance;