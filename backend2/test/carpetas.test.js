"use strict";

const carpetas = require('../index');
const connection = require('../Config/mysql');
const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = chai.expect

describe('Crear Carpeta', () => {
	it('Debería de crear la carpeta \'moramaz/Carpeta_Test\'', (done) => {
		chai.use(chaiHttp);
		chai.request(carpetas)
		.post('/carpetas/crear')
		.send({ruta_actual: 'moramaz/', nueva: 'Carpeta_Test', id_usuario: '43'})
		.end(function(err, res) {
			expect(err).to.be.null;
			expect(res).to.not.be.null;
			expect(res).to.have.status(200);
			expect(res.body).to.be.a('object');
			expect(res.body).to.have.property('status').eql(true);
			expect(res.body).to.have.property('message').eql('Carpeta creada en: \'moramaz/Carpeta_Test\'.');
			done();
    	});
	});
});

describe('Actualizar Carpeta', () => {
	it('Debería de renombrar la carpeta \'moramaz/Carpeta_Test\' a \'moramaz/Test\'', (done) => {
		chai.use(chaiHttp);
		chai.request(carpetas)
		.put('/carpetas/actualizar')
		.send({ruta_actual: 'moramaz/', carpeta: 'Carpeta_Test', nombre_nuevo: 'Test', id_usuario: '43'})
		.end(function(err, res) {
			expect(err).to.be.null;
			expect(res).to.not.be.null;
			expect(res).to.have.status(200);
			expect(res.body).to.be.a('object');
			expect(res.body).to.have.property('status').eql(true);
			expect(res.body).to.have.property('message').eql('Se ha modificado el nombre de la carpeta \'Carpeta_Test\' a \'Test\' de la ruta \'moramaz\'.');
			done();
    	});
	});
});

describe('Eliminar Carpeta', () => {
	let id_carpeta;
	
	before(async() => {
		var datos = await raw_query(`SELECT id_carpeta FROM Carpeta WHERE id_usuario = 43 AND nombre = 'Test';`)
		id_carpeta = datos[0].id_carpeta
	});

	it('Debería de eliminar la carpeta \'moramaz/Test\'', (done) => {
		chai.use(chaiHttp);
		chai.request(carpetas)
		.delete('/carpetas/eliminar/43/' + `${id_carpeta}`)
		.end(function(err, res) {
			expect(err).to.be.null;
			expect(res).to.not.be.null;
			expect(res).to.have.status(200);
			expect(res.body).to.be.a('object');
			expect(res.body).to.have.property('status').eql(true);
			expect(res.body).to.have.property('message').eql('Se ha eliminado la carpeta \'Test\'.');
			done();
    	});
	});
});

function raw_query(q) {
	return new Promise((resolve, reject) => {
		connection.query(q, (err, data) => {
			if (err) reject(err);
			resolve(data);
		});
	});
}