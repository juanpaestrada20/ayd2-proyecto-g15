let server = require('../index');
const Archivos = require('../Models/Archivos.js');
const archivos = new Archivos();

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let should = chai.should();

chai.use(chaiHttp);
describe('Pruebas del servicio de archivos', () => {
	const usuario = {
		nickname: 'userTest',
		correo: 'userTest@gmail.com',
		contra: 'userTest123',
		fecha_nacimiento: '20-07-1999',
		tipo: 0,
	};

	before(async () => {
		//Crea el usuario con el que se realizara la prueba
		let query =
			"INSERT INTO Usuario(nickname, correo, contra, fecha_nacimiento, tipo) VALUES (?, ?, ?, DATE_FORMAT(STR_TO_DATE(?, '%d-%m-%Y'), '%Y-%m-%d'), ?)";
		let params = [
			usuario.nickname,
			usuario.correo,
			usuario.contra,
			usuario.fecha_nacimiento,
			usuario.tipo,
		];
		await archivos.execQuery(query, params);
		// Obtiene el id_usuario
		query = 'SELECT id_usuario FROM Usuario WHERE nickname = ?';
		params = [usuario.nickname];
		let result = await archivos.execQuery(query, params);
		usuario.id_usuario = result.result[0].id_usuario;
		// Crea su carpeta
		query =
			'INSERT INTO Carpeta (nombre, fecha_creacion, id_usuario) VALUES ("CarpetaUsuarioPrueba", NOW(), ?)';
		params = [usuario.id_usuario];
		await archivos.execQuery(query, params);
		//Obtiene el id de la carpeta
		query =
			'SELECT id_carpeta FROM Carpeta WHERE nombre = "CarpetaUsuarioPrueba" AND id_usuario = ?';
		params = [usuario.id_usuario];
		result = await archivos.execQuery(query, params);
		usuario.id_carpeta = result.result[0].id_carpeta;
	});
	/*
	 * Ruta a testear /archivos/upload/:id_usuario/:id_carpeta con metodo POST
	 */
	describe('/archivos/upload/:id_usuario/:id_carpeta POST', () => {
		it('Deberia de subirse el archivo', (done) => {
			chai.request(server)
				.post(`/archivos/upload/${usuario.id_usuario}/${usuario.id_carpeta}`)
                .set('Content-Type', 'text/markdown')
				.attach(
					'file',
					`${__dirname.replace('/test','')}/API.md`,
					'API.md'
				)
				.end((err, res) => {
					console.log('got the res');
					if (err) {
						console.error(err);
					} else {
						res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('savedFiles');
                        res.body.savedFiles.should.be.a('array');
                        res.body.should.have.property('unsavedFiles');
                        res.body.unsavedFiles.should.be.a('array');
                        res.body.savedFiles.length.should.be.eql(1);
                        res.body.unsavedFiles.length.should.be.eql(0);
						usuario.id_archivo = res.body.savedFiles[0].id_archivo;
					}
					done();
				});
		});
	});

	/*
	 * Ruta a testear /archivos/updateName con metodo PUT
	 */
	describe('/archivos/updateName PUT', () => {
		it('Deberia de cambiarse el nombre del archivo', (done) => {
			const body ={
				"id_archivo": usuario.id_archivo,
				"nombre_nuevo": "nuevo nombre.md",
				"id_carpeta": usuario.id_carpeta
			}
			chai.request(server)
				.put(`/archivos/updateName`)
				.send(body)
				.end((err, res) => {
					if (err) {
						console.error(err);
					} else {
						res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('success').eql(true);
                        res.body.should.have.property('msg').eql("Nombre de archivo actualizado!");
					}
					done();
				});
		});
	});

	/*
	 * Ruta a testear /archivos/:id_usuario/:id_archivo con metodo DELETE
	 */
	describe('/archivos/:id_usuario/:id_archivo DELETE', () => {
		it('Deberia de eliminarse el archivo', (done) => {
			chai.request(server)
				.delete(`/archivos/${usuario.id_usuario}/${usuario.id_archivo}`)
				.end((err, res) => {
					if (err) {
						console.error(err);
					} else {
						res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('success').eql(true);
                        res.body.should.have.property('msg').eql("Archivo borrado exitosamente!");
					}
					done();
				});
		});
	});

	after(async () => {
		//Elimina el usuario al finalizar test
		let query = 'DELETE FROM Usuario WHERE nickname = ?';
		let params = [usuario.nickname];
		await archivos.execQuery(query, params);
		//Elimina la carpeta al finalizar test
		query = 'DELETE FROM Carpeta WHERE id_carpeta = ?';
		params = [usuario.id_carpeta];
		await archivos.execQuery(query, params);
	});
});
