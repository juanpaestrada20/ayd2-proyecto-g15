const express = require('express');
const cors = require('cors');
const {api} = require('./Routes/usuarios.routes');
const routesArchivos = require('./Routes/archivos.routes');
const routesCarpetas = require('./Routes/carpetas.routes');

const { json, urlencoded } = express;
const app = express();

const corsOptions = {
	origin: '*',
	optionsSuccessStatus: 200,
};

app.use(json());
app.use(urlencoded({ extended: false }));
app.use(cors(corsOptions));
app.use('/archivos', routesArchivos);
app.use('/carpetas', routesCarpetas);
app.use('/', api);

module.exports = app;
