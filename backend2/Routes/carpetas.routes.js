const express = require('express');
const router = express.Router();
const connection = require('../Config/db_instance');

async function crear_carpeta(req, res) {
	try {
		const { ruta_actual, nueva, id_usuario } = req.body;
		var carpetas = ruta_actual.split('/');
		var ruta_padre = carpetas[0];
		var { id_carpeta, ruta_padre } = await verificarCarpetas(ruta_actual);
		if (id_carpeta == -1) {
			res.json({ status: false, message: ruta_padre });
			return;
		}
		var id_padre = id_carpeta;
		var id_carpeta = await verificarCarpeta(nueva, id_carpeta);
		var msj = '';
		if (id_carpeta != -1) {
			msj =
				"La carpeta '" +
				nueva +
				"' ya existe en la ruta '" +
				ruta_padre +
				"/'.";
			console.log(msj);
			res.json({ status: false, message: msj });
			return;
		}
		var id_usr = await verificarUsuario(id_usuario);
		if (id_usr == -1) {
			msj = 'No se encontró el usuario especificado.';
			console.log(msj);
			res.json({ status: false, message: msj });
			return;
		}
		await raw_query(
			`INSERT INTO Carpeta (nombre, fecha_creacion, id_padre, id_usuario)
       VALUES ('${nueva}', NOW(), '${id_padre}', '${id_usuario}');`
		);
		msj = "Carpeta creada en: '" + ruta_padre + '/' + nueva + "'.";
		console.log(msj);
		res.json({ status: true, message: msj });
	} catch (error) {
		res.json({ status: false, message: error.message });
	}
}

async function verificarCarpetas(ruta) {
	try {
		var carpetas = ruta.split('/');
		var ruta_padre = carpetas[0];
		var id_carpeta = await verificarCarpeta(ruta_padre, -1);
		var msj = '';
		var slash = ruta[ruta.length - 1] == '/' ? 1 : 0;
		if (id_carpeta == -1) {
			msj = "La carpeta '" + ruta_padre + "' no fue encontrada.";
			return { id_carpeta: -1, ruta_padre: msj };
		}
		for (i = 1; i < Object.keys(carpetas).length - slash; i++) {
			id_carpeta = await verificarCarpeta(carpetas[i], id_carpeta);
			if (id_carpeta == -1) {
				msj =
					"La carpeta '" +
					carpetas[i] +
					"' no fue encontrada en la ruta '" +
					ruta_padre +
					"/'.";
				return { id_carpeta: -1, ruta_padre: msj };
			}
			ruta_padre = ruta_padre + '/' + carpetas[i];
		}
		return {
			id_carpeta,
			ruta_padre,
		};
	} catch (error) {
		return {
			id_carpeta: -1,
			ruta_padre: error.message,
		};
	}
}

async function verificarCarpeta(nombre, id_padre) {
	const query =
		id_padre == -1
			? `SELECT id_carpeta FROM Carpeta WHERE id_padre is NULL AND nombre = '${nombre}'`
			: `SELECT id_carpeta FROM Carpeta WHERE id_padre = '${id_padre}' AND nombre = '${nombre}'`;
	try {
		const datos = await raw_query(query);
		console.log(datos);
		return datos[0] == undefined ? -1 : datos[0].id_carpeta;
	} catch (error) {
		console.log(error);
		return -1;
	}
}

async function verificarUsuario(id_usuario) {
	const query = `SELECT id_usuario FROM Usuario WHERE id_usuario = '${id_usuario}'`;
	try {
		const datos = await raw_query(query);
		return datos[0] == undefined ? -1 : datos[0].id_usuario;
	} catch (error) {
		return -1;
	}
}

async function verificarCarpetaID(id_carpeta) {
	const query = `SELECT id_carpeta FROM Carpeta WHERE id_carpeta = '${id_carpeta}'`;
	try {
		const datos = await raw_query(query);
		return datos[0] == undefined ? -1 : datos[0].id_carpeta;
	} catch (error) {
		return -1;
	}
}

async function obtener_carpetas(req, res) {
	try {
		var id_usuario = req.params.id_usuario;
		var id_carpeta = req.params.id_carpeta;
		var msj = '';
		id_carpeta = await verificarCarpetaID(id_carpeta);
		if (id_carpeta == -1) {
			msj = 'No se ha encontrado la carpeta especificada.';
			console.log(msj);
			res.json({ status: false, message: msj });
			return;
		}
		id_usuario = await verificarUsuario(id_usuario);
		if (id_usuario == -1) {
			msj = 'No se encontró el usuario especificado.';
			console.log(msj);
			res.json({ status: false, message: msj });
			return;
		}
		var datos = await raw_query(
			`SELECT c.id_carpeta, c.nombre, c.fecha_creacion, d.cantidad_archivos
       FROM Carpeta c JOIN (SELECT c.id_carpeta, COUNT(a.id_archivo) as cantidad_archivos
       FROM Archivo a RIGHT JOIN Carpeta c
       ON a.id_carpeta = c.id_carpeta
       GROUP BY c.id_carpeta) d
       ON c.id_carpeta = d.id_carpeta
       WHERE id_padre = '${id_carpeta}';`
		);
		return res.json({ status: true, carpetas: datos });
	} catch (error) {
		res.json({ status: false, message: error.message });
	}
}

async function obtener_root(req, res) {
	try {
		var id_usuario = req.params.id_usuario;
		var id_usr = await verificarUsuario(id_usuario);
		if (id_usr == -1) {
			msj = 'No se encontró el usuario especificado.';
			console.log(msj);
			res.json({ status: false, message: msj });
			return;
		}
		var datos = await raw_query(
			`SELECT c.id_carpeta, c.nombre, c.fecha_creacion, d.cantidad_archivos
       FROM Carpeta c JOIN (SELECT c.id_carpeta, COUNT(a.id_archivo) as cantidad_archivos
       FROM Archivo a RIGHT JOIN Carpeta c
       ON a.id_carpeta = c.id_carpeta
       GROUP BY c.id_carpeta) d
       ON c.id_carpeta = d.id_carpeta
       WHERE id_padre is null AND c.id_usuario = '${id_usuario}';`
		);
		return res.json({ status: true, carpetas: datos });
	} catch (error) {
		res.json({ status: false, message: error.message });
	}
}

async function actualizar_carpeta(req, res) {
	try {
		const { ruta_actual, carpeta, nombre_nuevo, id_usuario } = req.body;
		var { id_carpeta, ruta_padre } = await verificarCarpetas(ruta_actual);
		if (id_carpeta == -1) {
			res.json({ status: ruta_padre });
			return;
		}
		var id_padre = id_carpeta;
		var msj = '';
		id_carpeta = await verificarCarpeta(carpeta, id_padre);
		if (id_carpeta == -1) {
			msj =
				"No se encontró la carpeta '" +
				carpeta +
				"' en la ruta '" +
				ruta_actual +
				"'.";
			console.log(msj);
			res.json({ status: false, message: msj });
			return;
		}
		var id_carpeta = await verificarCarpeta(nombre_nuevo, id_padre);
		if (id_carpeta != -1) {
			msj =
				"La carpeta '" +
				nombre_nuevo +
				"' ya existe en la ruta '" +
				ruta_padre +
				"/'.";
			console.log(msj);
			res.json({ status: false, message: msj });
			return;
		}
		var id_usr = await verificarUsuario(id_usuario);
		if (id_usr == -1) {
			msj = 'No se encontró el usuario especificado.';
			console.log(msj);
			res.json({ status: false, message: msj });
			return;
		}
		await raw_query(
			`UPDATE Carpeta
       SET nombre = '${nombre_nuevo}'
       WHERE id_padre = '${id_padre}' AND nombre = '${carpeta}';`
		);
		msj =
			"Se ha modificado el nombre de la carpeta '" +
			carpeta +
			"' a '" +
			nombre_nuevo +
			"' de la ruta '" +
			ruta_padre +
			"'.";
		console.log(msj);
		res.json({ status: true, message: msj });
	} catch (error) {
		res.json({ status: false, message: error.message });
	}
}

async function eliminar_carpeta(req, res) {
	try {
		var id_usuario = req.params.id_usuario;
		var id_carpeta = req.params.id_carpeta;
		var msj = '';
		id_carpeta = await verificarCarpetaID(id_carpeta);
		if (id_carpeta == -1) {
			msj = 'No se ha encontrado la carpeta especificada.';
			console.log(msj);
			res.json({ status: false, message: msj });
			return;
		}
		id_usuario = await verificarUsuario(id_usuario);
		if (id_usuario == -1) {
			msj = 'No se encontró el usuario especificado.';
			console.log(msj);
			res.json({ status: false, message: msj });
			return;
		}
		var datos = await raw_query(
			`SELECT nombre
       FROM Carpeta
       WHERE id_carpeta = '${id_carpeta}';`
		);
		await raw_query(
			`DELETE FROM Carpeta
       WHERE id_carpeta = '${id_carpeta}';`
		);
		msj = "Se ha eliminado la carpeta '" + datos[0].nombre + "'.";
		console.log(msj);
		res.json({ status: true, message: msj });
	} catch (error) {
		res.json({ status: false, message: error.message });
	}
}

function wrap_function(anotherFunction, debugInfo = null) {
	return function (req, res) {
		if (debugInfo) console.log(debugInfo);
		anotherFunction(req, res).catch((err) => {
			return res.status(500).send({
				message: err['message'] ? err['message'] : 'An error occurred.',
			});
		});
	};
}

function raw_query(q) {
	return connection.getSingleton().execQuery(q, null);
}

/* Métodos HTTP */
router.post('/crear', wrap_function(crear_carpeta));

router.get('/obtener/:id_usuario/:id_carpeta', wrap_function(obtener_carpetas));
router.get('/obtener/:id_usuario', wrap_function(obtener_root));

router.put('/actualizar', wrap_function(actualizar_carpeta));

router.delete(
	'/eliminar/:id_usuario/:id_carpeta',
	wrap_function(eliminar_carpeta)
);

module.exports = router;
