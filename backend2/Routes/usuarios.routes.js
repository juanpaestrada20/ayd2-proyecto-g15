const express = require('express');
const api = express.Router();
const env = require('../Config/config');
const connection = require('../Config/db_instance');
const {sendEmail} = require('../Config/email');
const crypto = require('crypto');
const client = require('twilio')(env.TWILIO_ACCOUNT_SID, env.TWILIO_AUTH_TOKEN);

api.post('/user', async (req, res) => {
	const { nickname, correo, phone, contra, fecha_nacimiento, tipo } =
		req.body;
	const query =
		"INSERT INTO Usuario(nickname, correo, phone, contra, fecha_nacimiento, tipo) VALUES (?, ?, ?, ?, DATE_FORMAT(STR_TO_DATE(?, '%d-%m-%Y'), '%Y-%m-%d'), ?)";
	const params = [nickname, correo, phone, contra, fecha_nacimiento, tipo];
	try {
		const result = await execQuery(query, params);
		if (result.success)
			res.send({
				success: true,
				msg: 'Usuario creado!',
				id: result.result.insertId,
			});
		else throw result.error;
	} catch (err) {
		if (err.sqlMessage.includes('nickname'))
			res.send({
				success: true,
				error: 'El nickname ya esta en uso!',
			});
		else if (err.sqlMessage.includes('correo'))
			res.send({
				success: false,
				error: 'El correo ya esta en uso!',
			});
		else res.status(500).json(err);
	}
});

api.put('/user', async (req, res) => {
	const {
		nickname,
		correo,
		phone,
		fecha_nacimiento,
		contra,
		tipo,
		auth_email,
		auth_phone,
	} = req.body;
	const query =
		"UPDATE Usuario SET correo = ?, phone = ?, fecha_nacimiento = DATE_FORMAT(STR_TO_DATE(?, '%d-%m-%Y'), '%Y-%m-%d'), tipo = ?, auth_email = ?, auth_phone = ? WHERE nickname = ? AND contra = ?";
	const params = [
		correo,
		phone,
		fecha_nacimiento,
		tipo,
		auth_email,
		auth_phone,
		nickname,
		contra,
	];
	try {
		const result = await execQuery(query, params);
		if (result.success)
			result.result.changedRows > 0
				? res.send({ success: true, msg: 'Usuario actualizado!' })
				: res.send({
						success: false,
						msg: 'Ningún usuario fue actualizado!',
				  });
		else throw result.error;
	} catch (err) {
		res.status(500).json(err);
	}
});

api.put('/user/contra', async (req, res) => {
	const { nickname, contra, contra_nueva } = req.body;
	const resultExistencia = await verifyExistency(nickname, contra);
	try {
		if (!resultExistencia.success && resultExistencia.error)
			throw resultExistencia.error;
		if (!resultExistencia.success && resultExistencia.msg)
			res.send({ success: false, msg: resultExistencia.msg });
		else {
			const query = 'UPDATE Usuario SET contra = ? WHERE id_usuario = ?';
			const params = [contra_nueva, resultExistencia.id_usuario];
			const result = await execQuery(query, params);
			if (result.success)
				result.result.changedRows > 0
					? res.send({
							success: true,
							msg: 'Contraseña actualizada!',
					  })
					: res.send({
							success: false,
							msg: 'Ninguna contraseña fue actualizada!',
					  });
			else throw result.error;
		}
	} catch (err) {
		res.status(500).json(err);
	}
});

const verifyExistency = async (nickname, contra) => {
	const query =
		'SELECT id_usuario FROM Usuario WHERE nickname = ? AND contra = ?';
	const params = [nickname, contra];
	try {
		const result = await execQuery(query, params);
		if (result.success)
			return result.result[0]
				? { success: true, id_usuario: result.result[0].id_usuario }
				: { success: false, msg: 'Usuario no encontrado!' };
		else throw result.error;
	} catch (err) {
		return { success: false, error };
	}
};

api.post('/login', async (req, res) => {
	const { nickname, contra } = req.body;
	const query = `SELECT id_usuario, nickname, correo, phone, DATE_FORMAT(fecha_nacimiento, '%d-%m-%Y') as fecha_nacimiento, tipo, auth_email, auth_phone FROM Usuario WHERE nickname = ? AND contra = ?`;
	const params = [nickname, contra];
	try {
		const result = await execQuery(query, params);
		console.log(result);
		if (result.success) {
			if (result.result[0])
				await saveToken(
					result.result[0].auth_phone,
					result.result[0].auth_email,
					result.result[0].id_usuario,
					result.result[0].correo,
					result.result[0].phone
				);
			result.result[0]
				? res.send({ user: result.result[0] })
				: res.send({ user: null, msg: 'Usuario no encontrado' });
		} else throw result.error;
	} catch (err) {
		res.status(500).json(err);
	}
});

api.post('/validate', async (req, res) => {
	const { id_usuario, token_email, token_phone } = req.body;
	try {
		const query =
			'SELECT auth_email, auth_phone, token_email, token_phone FROM Usuario WHERE id_usuario = ? AND NOW() < token_lifetime';
		const params = [id_usuario];
		const result = await execQuery(query, params);
		console.log(result);
		if (result.result[0]) {
			if (
				result.result[0].auth_email == 1 &&
				result.result[0].token_email != token_email
			)
				res.send({
					success: false,
					msg: 'Token de correo inválido!',
				});
			else if (
				result.result[0].auth_phone == 1 &&
				result.result[0].token_phone != token_phone
			)
				res.send({
					success: false,
					msg: 'Token de teléfono inválido!',
				});
			else
				res.send({
					success: true,
					msg: 'Token válido!',
				});
		} else
			res.send({
				success: false,
				msg: 'Tiempo de token inválido!',
			});
	} catch (err) {
		res.status(500).json(err);
	}
});

api.post('/resend', async (req, res) => {
	const { id_usuario, auth_email, auth_phone } = req.body;
	const query = 'SELECT correo, phone FROM Usuario WHERE id_usuario = ?';
	const params = [id_usuario];

	try {
		const usuario = await execQuery(query, params);
		if (usuario.success) {
			if (usuario.result[0]) {
				const result = await saveToken(
					auth_phone,
					auth_email,
					id_usuario,
					usuario.result[0].correo,
					usuario.result[0].phone
				);
				res.send({ success: true });
			}
			res.send({ success: false });
		} else throw usuario.error;
	} catch (err) {
		res.status(500).json(err);
	}
});

async function saveToken(auth_phone, auth_email, userId, email, phone) {
	let token_phone = null;
	let token_email = null;
	try {
		if (auth_phone == 1) {
			token_phone = generateToken();
			console.log(token_phone);
			try {
				client.messages
					.create({
						body: `Tu código es: ${token_phone}`,
						from: '+18508054238',
						to: phone.includes('+502')
							? phone.trim()
							: `+502${phone.trim()}`,
					})
					/* .then((message) => console.log(message.sid)) */
					.catch((e) => console.log(e));
			} catch (e) {
				console.log(e);
			}
		}
		if (auth_email == 1) {
			token_email = generateToken();
			console.log(token_email);
			await sendEmail(token_email, email);
		}
		const query = `UPDATE Usuario SET token_email = ?, token_phone = ?, token_lifetime = DATE_ADD(NOW(), INTERVAL 3 MINUTE) WHERE id_usuario = ?`;
		const params = [token_email, token_phone, userId];
		const result = await execQuery(query, params);
        console.log('Resultado');
        console.log(result);
		return result.success;
	} catch (err) {
        console.log(err);
		return false;
	}
}

function generateToken() {
	return crypto.randomBytes(20).toString('hex').slice(0, 6);
}

api.delete('/user', async (req, res) => {
	const { nickname, contra } = req.body;
	const query = 'DELETE FROM Usuario WHERE nickname = ? AND contra = ?';
	const params = [nickname, contra];
	try {
		const result = await execQuery(query, params);
		if (result.success)
			result.result.affectedRows > 0
				? res.send({ success: true, msg: 'Usuario eliminado!' })
				: res.send({
						success: false,
						msg: 'Ningún usuario fue eliminado!',
				  });
		else throw result.error;
	} catch (err) {
		res.status(500).json(err);
	}
});

function execQuery(databaseQuery, params) {
	return connection.getSingleton().execQuery(databaseQuery, params);
}

module.exports = { api, execQuery };
