cp /home/config/backend/.env ./backend2/.env
cp /home/config/frontend/.env ./web-app/.env

cp ./backend2/prod/Dockerfile ./backend2/Dockerfile
sudo docker build -t alesandrog/backend:$COMMIT  ./backend2 --no-cache 
sudo docker push alesandrog/backend:$COMMIT  
sudo docker build -t alesandrog/frontend:$COMMIT  ./web-app --no-cache 
sudo docker push alesandrog/frontend:$COMMIT

sudo docker rmi alesandrog/backend:$COMMIT -f
sudo docker rmi alesandrog/frontend:$COMMIT -f


