# Fase 3



## Kubernetes 


Se planteó una arquitectura en la que se describía para la aplicación cliente un ReplicaSet con 2 réplicas y un LoadBalancer, de manera similar para el servidor se levantó un ReplicaSet con 2 replicas y un LoadBalancer.


Toda la arquitectura fue creada usando el "Google Kubernetes Engine" de Google Cloud Plattform.

![kub](https://user-images.githubusercontent.com/55900600/116884019-7c85c700-abe3-11eb-94fb-ca07190db15f.PNG)

![ingresss](https://user-images.githubusercontent.com/55900600/116885876-a0e2a300-abe5-11eb-9259-45682d0d0b9d.PNG)

## Terminología

**Pod**: Unidad básica en Kubernetes.

**ReplicaSet**: Un ReplicaSet es un controlador encargado de verificar que la cantidad de pods deseada e indicada se mantenga activa en el clúster.

**Deployment**: Describe el estado deseado del clúster y proporciona actualizaciones declarativas para el estado de los Pods y ReplicaSets.

**Rolling Updates**: Permiten actualizaciones de Deployments sin tiempo de baja, consiguiendo reemplazar pods incrementalmente con una versión más nueva.

Esta estrategia fue aplicada en el desarrollo del proyecto para cargar actualizaciones de la aplicación

